### Create Manual Charge

_**Create Manual Charge**_ - this feature is used to process payment from the customer's registered credit card on the app.

* Only the default credit card of one of our Active customer can be used.

* Use this with caution and with customer's consent only. 

* Any amount can be processed through this function using stripe.

### How to Process Manual Payment

1. Click _**Stripe**_.

	![Stripe](/img/stripeman.png) <br></br>

2. Click _**Create Manual Charge**_.

	![Stripe](/img/stripeman1.png) <br></br>

	Then you will be directed to this page.

	![Stripe](/img/stripeman2.png) <br></br>

3. Choose the user you want to charge, input the amount you want to collect and put in the description for the payment or notes about the payment.

	![Stripe](/img/stripeman3.png) <br></br>

4. Click _**Create Manual Charge**_.

	![Stripe](/img/stripeman4.png) <br></br>

	Then you will be directed to this page.

	![Stripe](/img/stripeman5.png) <br></br>

### How to View Manual Payment 

1. Click _**Stripe**_.

	![Stripe](/img/stripeman.png) <br></br>

2. Click _**Stripe Transactions**_.

	![Stripe](/img/stripeman6.png) <br></br>

	Then you will be directed to this page.

	![Stripe](/img/stripeman7.png) <br></br>

	Here you can see all the transactions you did manually using Stripe, the transfer made to manually add stripe balance and all the manual collection made via Stripe Manual Charge.