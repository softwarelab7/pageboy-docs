### Reports

* There are different types of report that can be generated from the system.

	* _**Ordered Placed**_ - This report is where you will see all orders made on the app with their details, from the time duration you have put in on Date From and Date to. <br></br>

	* _**Product Analytics**_ - This report is where you will see all the products, how many times they are viewed and how many times they are ordered, from the time duration you have put in on Date From and Date to. This report is very useful to see what product sells most. <br></br>

	* _**User Analytics**_ - This report is where you will see all the users, date of registration, product views and their purchase made on the app, from the time duration you have put in on Date From and Date to. <br></br>

	* _**Open Baskets**_ - This report is where you will see all the open baskets or orders from your app, from the time duration you have put in on Date From and Date to. <br></br>

	* _**Submitted Baskets**_ - This report is where you will see all the submitted baskets or orders from your app, from the time duration you have put in on Date From and Date to. <br></br>

	* _**Voucher Performance**_ - This report is where you will see how many times your promotion/voucher was used and added to a basket. How much discount was used. This will reflect the promotion/voucher performance from the time duration you have put in on Date From and Date to. <br></br>

	* _**Offer Performance**_ - This reports is where you can see the promotions you have offered from the time duration you have put in on Date From and Date to. This report will show you the amount of discounts your promotion/voucher have reached. <br></br>

	* _**Statistics**_ - This report is where you can see the breakdown of all the orders made on your app. How many are Confirmed, cancelled, pending and processed. This includes the total revenue and the total orders made on the app. <br></br>


### Ordered Placed

_**Ordered Placed**_ - This report is where you will see all orders made on the app with their details, from the time duration you have put in on Date From and Date to. <br></br>

1. Click _**Reports**_

	![Reports](/img/reports.png) <br></br>

	You will be directed to this page.

	![Reports](/img/reports1.png) <br></br>

2. Enter a Date on _**Date from**_ and _**Date to**_.

	![Reports](/img/reports2.png) <br></br>

3. Click _**Generate Report**_.

	![Reports](/img/reports3.png) <br></br>

	Reports will be generated on the dashboard.

	![Reports](/img/reports4.png) <br></br>

4. Click _**View**_  beside the order to be able to see the order details.

	![Reports](/img/reports5.png) <br></br>

	Here is the view of the order details.

	![Reports](/img/reports6.png) <br></br>


### Product Analytics

_**Product Analytics**_ - This report is where you will see all the products, how many times they are viewed and how many times they are ordered, from the time duration you have put in on Date From and Date to. This report is very useful to see what product sells most. <br></br>

1. Click _**Reports**_

	![Reports](/img/reports.png) <br></br>

	You will be directed to this page.

	![Reports](/img/reports1.png) <br></br>

2. Enter a Date on _**Date from**_ and _**Date to**_.

	![Reports](/img/reports2.png) <br></br>

3. Click _**Report Type**_, choose _**Product Analytics**_.

	![Reports](/img/reports7.png) <br></br

4. Click _**Generate Report**_.

	![Reports](/img/reports13.png) <br></br>

	Reports will be generated on the dashboard.

	![Reports](/img/reports14.png) <br></br>



### User Analytics

_**User Analytics**_ - This report is where you will see all the users, date of registration, product views and their purchase made on the app, from the time duration you have put in on Date From and Date to. <br></br>

1. Click _**Reports**_

	![Reports](/img/reports.png) <br></br>

	You will be directed to this page.

	![Reports](/img/reports1.png) <br></br>

2. Enter a Date on _**Date from**_ and _**Date to**_.

	![Reports](/img/reports2.png) <br></br>

3. Click _**Report Type**_, choose _**User Analytics**_.

	![Reports](/img/reports8.png) <br></br>

4. Click _**Generate Report**_.

	![Reports](/img/reports15.png) <br></br>

	Reports will be generated on the dashboard.

	![Reports](/img/reports16.png) <br></br>




### Open Baskets

_**Open Baskets**_ - This report is where you will see all the open baskets or orders from your app, from the time duration you have put in on Date From and Date to. <br></br>

	
1. Click _**Reports**_

	![Reports](/img/reports.png) <br></br>

	You will be directed to this page.

	![Reports](/img/reports1.png) <br></br>

2. Enter a Date on _**Date from**_ and _**Date to**_.

	![Reports](/img/reports2.png) <br></br>

3. Click _**Report Type**_, choose _**Open baskets**_.

	![Reports](/img/reports9.png) <br></br>

4. Click _**Generate Report**_.

	![Reports](/img/reports17.png) <br></br>

	Reports will be generated on the dashboard.

	![Reports](/img/reports18.png) <br></br>




### Submitted Baskets

_**Submitted Baskets**_ - This report is where you will see all the submitted baskets or orders from your app, from the time duration you have put in on Date From and Date to. <br></br>



1. Click _**Reports**_

	![Reports](/img/reports.png) <br></br>

	You will be directed to this page.

	![Reports](/img/reports1.png) <br></br>

2. Enter a Date on _**Date from**_ and _**Date to**_.

	![Reports](/img/reports2.png) <br></br>

3. Click _**Report Type**_, choose _**Submitted baskets**_.

	![Reports](/img/reports10.png) <br></br>


4. Click _**Generate Report**_.

	![Reports](/img/reports19.png) <br></br>

	Reports will be generated on the dashboard.

	![Reports](/img/reports20.png) <br></br>


### Voucher Performance

_**Voucher Performance**_ - This report is where you will see how many times your promotion/voucher was used and added to a basket. How much discount was used. This will reflect the promotion/voucher performance from the time duration you have put in on Date From and Date to. <br></br>



1. Click _**Reports**_

	![Reports](/img/reports.png) <br></br>

	You will be directed to this page.

	![Reports](/img/reports1.png) <br></br>

2. Enter a Date on _**Date from**_ and _**Date to**_.

	![Reports](/img/reports2.png) <br></br>

3. Click _**Report Type**_, choose _**Voucher Performance**_.

	![Reports](/img/reports11.png) <br></br>

4. Click _**Generate Report**_.

	![Reports](/img/reports21.png) <br></br>

	Reports will be generated on the dashboard.

	![Reports](/img/reports22.png) <br></br>



### Offer Performance

_**Offer Performance**_ - This reports is where you can see the promotions you have offered from the time duration you have put in on Date From and Date to. This report will show you the amount of discounts your promotion/voucher have reached. <br></br>



1. Click _**Reports**_

	![Reports](/img/reports.png) <br></br>

	You will be directed to this page.

	![Reports](/img/reports1.png) <br></br>

2. Enter a Date on _**Date from**_ and _**Date to**_.

	![Reports](/img/reports2.png) <br></br>

3. Click _**Report Type**_, choose _**Offer Performance**_.

	![Reports](/img/reports12.png) <br></br>

4. Click _**Generate Report**_.

	![Reports](/img/reports23.png) <br></br>

	Reports will be generated on the dashboard.

	![Reports](/img/reports24.png) <br></br>


### Statistics

_**Statistics**_ - This report is where you can see the breakdown of all the orders made on your app. How many are Confirmed, cancelled, pending and processed. This includes the total revenue and the total orders made on the app. <br></br>

1. Click _**Fulfillment**_.

	![coverage](/img/coverage.png) <br></br>

2. Click _**Statistics**_.

	![Reports](/img/reports25.png) <br></br>

	Here you will see this report below.

	![Reports](/img/reports25.png) <br></br>