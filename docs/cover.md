### Coverage

* Coverage - These are the list of Zip Codes covered by Pageboy App. These Zip Codes represents the coverage of the services or products of Pageboy app. <br></br>


	1. First, log in using your Admin credentials.

	2. Click _**Fulfillment**_.

		![coverage](/img/coverage.png) <br></br>

	3. Click _**Coverage**_.

		![coverage](/img/coverage1.png) <br></br>

		You will be directed to a page where you can see all the list of Zip Codes Registered on the system.

		![coverage](/img/coverage2.png) <br></br>

		You will see Zip Code, City, State, Country and Action. <br></br>

Note: If you add zip code, the location it covers will be automatically covered by your Pageboy App. Deleting a Zip code will automatically remove that location from the app coverage.  <br></br>


### Adding a New Zip Code to Coverage

1. First, log in using your Admin credentials.

2. Click _**Fulfillment**_.

	![coverage](/img/coverage.png) <br></br>

3. Click _**Coverage**_.

	![coverage](/img/coverage1.png) <br></br>

	You will be directed to a page where you can see all the list of Zip Codes Registered on the system.

	![coverage](/img/coverage2.png) <br></br>

4. Enter a Zip code on _**Zipcode box**_. Then Click _**+Add to coverage**_.

	![coverage](/img/cover3.png) <br></br>

5. Since Zip Code 10001 is for New York City, the system will automatically populate the details of the Zip Code and it will be automatically added to the list of Coverage.

	![coverage](/img/cover4.png) <br></br>


### How to Delete Zip Code from Coverage

1. First, log in using your Admin credentials.

2. Click _**Fulfillment**_.

	![coverage](/img/coverage.png) <br></br>

3. Click _**Coverage**_.

	![coverage](/img/coverage1.png) <br></br>

	You will be directed to a page where you can see all the list of Zip Codes Registered on the system.

	![coverage](/img/coverage2.png) <br></br>

4. Click _**Remove**_ to delete the zip code from your coverage.

	![coverage](/img/Coverage5.png) <br></br>

	You will be directed to a page where you are asked if you really want to remove the Zip Code.

	![coverage](/img/coverage6.png) <br></br>

5. Click _**Delete**_, this will permanently delete the zip code from your coverage.

	![coverage](/img/coverage7.png) <br></br>


