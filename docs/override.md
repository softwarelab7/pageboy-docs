### Admin Schedule Override Feature

This feature is used to override all appointments that are not yet finished or started. Admin can edit the following items using this feature:

* **Time of appointment**.

* **Address of the appointment**.

* **Stylist of the appointment**, regardless if the stylist is on off or is under unavailable schedule and regardless if the stylist have conflicting schedule.

* **Notes** are also shown on the order to show who edited the appointment, just follow [How to View Order Details of Specific Order](#how-to-view-order-details-of-specific-order)

### How to use Override Feature

1. Click _**Fulfillment**_.

	![Override](/img/over.png) <br></br>

2. Click _**Appointments**_.

	![Override](/img/over1.png) <br></br>

	Then you will be directed to this page.

	![Override](/img/over2.png) <br></br>

3. Make sure to have the _**Appointment filter**_ to _**Upcoming**_. Click _**Actions**_.

	![Override](/img/over3.png) <br></br>

	You will see the following choices: **View** and **Edit**.

	![Override](/img/over4.png) <br></br>

4. Click _**Edit**_.

	![Override](/img/over5.png) <br></br>

	Then you will be directed to this page.

	![Override](/img/over6.png) <br></br>	

	From this page you can edit the time of the appointment Start and End. You can edit the address of the appointment and the stylist.

	**NOTE**: You will see the following when you are editing an appointment and there is a conflict with the stylist schedule indicating that there will be conflict of schedule for stylist.

	![Override](/img/over7.png) <br></br>	

	**Please be careful in assigning stylist, you will only see this warning if the stylist have conflicting schedule. Stylists choices are based on their skill, regardless if the stylist is on off or filed an unavailable schedule** <br></br>

5. Click _**Submit**_ to confirm the changes.

	![Override](/img/over8.png) <br></br>	

	Then you will be directed to this page.

	![Override](/img/over9.png) <br></br>