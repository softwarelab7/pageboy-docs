### Time Padding

There are 2 types of time padding that we have on this system.

1. _**Appointment Padding**_ - This is the time interval for booking appointments. It is normally set in minutes and used for booking time interval from one Booking schedule to the next booking schedule.

	* Example: Appointment padding is set to 15 minutes. The booking times will start at 7am-8am, the next booking schedule will be 8:15am to 9:15am, the next one will be 9:30am to 10:30am and so on and so forth.  <br></br>

2. _**Set Padding From Current Date and Time**_ - This is the time interval for a user before he/she would be able to book an appointment from the current time. It is normally set in minutes.

	* Example: The time as of now is 7am and you are using your Pageboy App, if the Set Padding From Current Date and Time is 60 minutes, you can only book 8am schedules onward. <br></br>

### How to Set Appointment Padding

1. Click _**Fulfillment**_.

	![coverage](/img/coverage.png) <br></br>

2. Click _**Appointment Padding**_.

	![Time Padding](/img/timepadding.png) <br></br>

	You will be directed to this page.

	![Time Padding](/img/timepadding1.png) <br></br>

3. Set the time interval or padding you want to set per booking by entering the number of minutes the padding will be.

	![Time Padding](/img/timepadding2.png) <br></br>

4. Click _**Save**_. This will save the time padding interval per booking.

	![Time Padding](/img/timepadding3.png) <br></br>

	You will see a confirmation message that the time padding or interval was saved.

	![Time Padding](/img/timepadding4.png) <br></br>


### How to Set Padding From Current Date and Time

1. Click _**Fulfillment**_.

	![coverage](/img/coverage.png) <br></br>

2. Click _**Set Padding From Current Date and Time**_.

	![Time Padding](/img/timepadding6.png) <br></br>

	You will be directed to this page.

	![Time Padding](/img/timepadding5.png) <br></br>

3. Set the time interval or padding you want to set before the a user can book a schedule from the current date and time by entering the number of minutes the padding will be.

	![Time Padding](/img/timepadding7.png) <br></br>

4. Click _**Save**_. This will save the time padding interval per booking.

	![Time Padding](/img/timepadding8.png) <br></br>

	You will see a confirmation message that the time padding or interval was saved.

	![Time Padding](/img/timepadding9.png) <br></br>