
### Logging In

* Go to Pageboy Admin site.

* Click _**Sign In**_.

	![log](/img/login.png) <br></br>

* Input admin credentials and click _**Log In**_.

	![log](/img/log1.png) <br></br>

	You will see _**Dashboard**_ as your landing page.

	![log](/img/login1.png) <br></br>




