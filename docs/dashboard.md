### Dashboard

* When you logged in as Admin of this website, you will see your _**Dashboard**_ as your landing page.

	The following are the content of your _**Dashboard**_ :

	1. Store Statistics

	2. Orders - Last 24 Hours

	3. Orders - All Time

	4. Customers

	5. Catalogue and Stocks

	6. Offers, vouchers and promotions <br></br>


### Store Statistics


* Store Statistics - Here you will see the Total Orders made on your IOS app, New Customers, Total Customers and Total Products you have. You will see the data within the day including the graph of orders within time intervals. See image below.

	![Dashboard](/img/dashboard.png) <br></br>

### Orders Last 24 Hours

* Orders - Last 24 Hours - Here you will see all the orders made on your IOS Pageboy app within 24 hrs. 

	![Dashboard](/img/dashboard1.png) <br></br>

### Orders All Time

* Orders - All Time - Here you will see the total orders made on your IOS app, from the day you started using the app up to present.

	![Dashboard](/img/dashboard2.png) <br></br>

### Customers

* Customers - Here you will see the total number of customers you have on your IOS app. You will also see if there are new customers within 24 hrs.

	![Dashboard](/img/dashboard3.png) <br></br>


### Catalogue and Stocks

* Catalogue and Stocks - Here you will see all the stocks you have and products offered.

	![Dashboard](/img/dashboard4.png) <br></br>

### Offers, vouchers and promotions

* Offers, vouchers and promotions - Here you will see all the active promotions you have, all discounts or vouchers you are offering that are currently active on the site.

	![Dashboard](/img/dashboard5.png) <br></br>