### Stripe Balance

* _**Stripe**_ - it is a payment gateway that handles the process the payments made by Pageboy customers to Pageboy Bank account. Weekly all cleared payments are transferred to Pageboy Bank Account. <br></br>

* _**Stripe Balance**_ - it is where we can see pending and processed payments from Pageboy customers. We need Stripe Balance to have certain amount daily to make sure that Stripe can process the payments for Gratuity of our Stylists on a daily basis depending on the services they catered.  <br></br>


### How to view Stripe Transactions

1. Click _**Stripe**_

	![Stripe](/img/stripe.png) <br></br>

2. Click _**Stripe Transactions**_.

	![Stripe](/img/stripe1.png) <br></br>

	You will be directed to this page.

	![Stripe](/img/stripe2.png) <br></br>

	Here you ca see the Stripe ID, Transaction Kind/Type, Amount, User or Name of the person who transferred the money and Transaction date. <br></br>


### How to Add Stripe Balance

* _**Stripe Balance**_ - it is where we can see pending and processed payments from Pageboy customers. We need Stripe Balance to have certain amount daily to make sure that Stripe can process the payments for Gratuity of our Stylists on a daily basis depending on the services they catered.  <br></br> 

	1. Click _**Stripe**_

		![Stripe](/img/stripe.png) <br></br>

	2. Click _**Add to Stripe Balance**_.

		![Stripe](/img/stripe3.png) <br></br>

	3. Click _**Use new card**_ to add a new card where you will get the money to be transferred to Stripe Balance.

		![Stripe](/img/stripe4.png) <br></br>

		
		* Fill out the details. Then click _**Add Balance**_.

			* Balance to add - the amount of money you want to transfer from your Bank Account to the Stripe Balance of the system.

			* Card Number - the credit card number to be used.

			* CVC - Card Verification Code of your credit card.

			* Expiration - expiration Month and Year of your credit card. <br></br>

		![Stripe](/img/stripe5.png) <br></br>

	4. Or if you have used a card before that you want to use again. Click _**Card**_, then choose the card you want to use, you will see the last 4 of the card and the card's expiration month and year.

		![Stripe](/img/stripe6.png) <br></br>

	5. Make sure to enter the amount you want to transfer from your Bank Account to the Stripe Bank Account. 

		![Stripe](/img/stripe7.png) <br></br>

	6. Click _**Add Balance**_.

		![Stripe](/img/stripe8.png) <br></br>

		You will be directed to this page

		![Stripe](/img/stripe9.png) <br></br>

	7. Click _**Yes**_ to confirm the transfer.

		![Stripe](/img/stripe10.png) <br></br>

		You will be directed to this page

		![Stripe](/img/stripe11.png) <br></br>


	Note: Please take note that there is a Stripe Transaction Fee depending on the amount being transferred.





			












