
# Welcome to Pageboy App Docs
This is the documentation on how to manage the administration site of Pageboy.

## Administration Guide

* [Getting Started](login)
	
	* [Logging In](login)<br></br>

* [Definition of Terms](definitionofterms)

	* [Dashboard](definitionofterms#dashboard)

	* [Appointment Status of an Order](definitionofterms#appointment-status-of-an-order)

	* [Time Padding](definitionofterms#time-padding)

	* [Promotions](definitionofterms#promotions)

	* [Reports](definitionofterms#reports)

	* [Stripe Balance](definitionofterms#stripe-balance)

	* [All about Stylist Management](definitionofterms#all-about-stylist-management)

	* [User Status](definitionofterms#user-status)

	* [Types of Users](definitionofterms#types-of-users) <br></br>


* [Dashboard](dashboard) 
	
	* [Store Statistics](dashboard#store-statistics)

	* [Orders - Last 24 Hours](dashboard#orders-last-24-hours)

	* [Orders - All Time](dashboard#orders-all-time) 

	* [Customers](dashboard#customers) 

	* [Catalogue and Stock](dashboard#catalogue-and-stocks)

	* [Offers, Vouchers and Promotions](dashboard#offers,-vouchers-and-promotions) <br></br>


* [Product Management/Catalogue](product)

	* [Looks Products/Services](product#looks-product-services)

	* [How to Add New Service or Product](product#how-to-add-new-service-or-product)

	* [How to Edit Product Information](product#how-to-edit-product-information)

	* [How to Delete Product/Service](product#how-to-delete-product-service) <br></br>



* [Order Management](order)

	* [How to View Orders](order#how-to-view-orders)

	* [How to View Order Details of Specific Order](order#how-to-view-order-details-of-specific-order)

	* [How to View Appointment Details of an Order](order#how-to-view-appointment-details-of-an-order)

	* [How to View All Appointments](order#how-to-view-all-appointments)

	* [Appointment Status of an Order](order#appointment-status-of-an-order)

	* [How to Cancel an Appointment via Web](order#how-to-cancel-an-appointment-via-web)

	* [How to Change Attending Stylist of a Certain Appointment](order#how-to-change-attending-stylist-of-a-certain-appointment) <br></br>



* [Users Management](users)

	* [Users Status](users#users-status)

	* [Types of Users](users#types-of-users)	

	* [User Management](users#user-management)

	* [How to Change User Status or Type](users#how-to-change-user-status-or-type) 

	* [Waitlist](users#waitlist) 

	* [How to do Password Reset](users#how-to-do-password-reset) <br></br>



* [All About Stylist](stylist)

	* [All About Stylist Management](stylist#all-about-stylist-management)

	* [Partners or Stylists](stylist#partners-or-stylists)

	* [Set Stylist Account As Reviewed](stylist#set-stylist-account-as-reviewed)

	* [How to Check Stylist Coverage](stylist#how-to-check-stylist-coverage)

	* [How to View Stylist Schedules](stylist#how-to-view-stylist-schedules)

	* [How to View Stylist Schedules](stylist#how-to-view-stylist-work-schedules)

	* [Stylist Bank Account](stylist#stylist-bank-account)

	* [How to Update Stylist Bank Account via Web](stylist#how-to-update-stylist-bank-account-via-web) <br></br>


* [Promotions](promo)

	* [Promotions](promo#promotions)

	* [How to Create Range for All Products](promo#how-to-create-range-for-all-products)

	* [How to Create Range for Specific Products or Services](promo#how-to-create-range-for-specific-products-or-services)

	* [How to Edit Ranges](promo#how-to-edit-ranges)

	* [How to Delete Ranges](promo#how-to-delete-ranges)

	* [Creating New Promo Code](promo#creating-new-promo-code)

	* [How to View Details of your Promo Code](promo#how-to-view-details-of-your-promo-code)

	* [How to Edit Promo Code](promo#how-to-edit-promo-code)

	* [How to Delete Promo Code](promo#how-to-delete-promo-code) 	<br></br>

* [Coverage](cover)

	* [Coverage](cover)

	* [Adding a Zip Code to Coverage](cover#adding-a-zip-code-to-coverage)

	* [How to Delete Zip Code from Coverage](cover#how-to-delete-zip-code-from-coverage) <br></br>


* [Schedules](admin/sched) <br></br>

* [Time Padding](padding) 

	* [Time Padding](padding) 

	* [How to Set Appointment Padding](padding#how-to-set-appointment-padding) 

	* [How to Set Padding From Current Date and Time](padding#how-to-set-padding-from-current-date-and-time) <br></br>

* [Reports and Statistics](reports) 

	* [Reports](reports#reports) 

	* [Ordered Placed](reports#ordered-placed) 

	* [Product Analytics](reports#product-analytics) 

	* [User Analytics](reports#user-analytics) 

	* [Open Baskets](reports#open-baskets) 

	* [Submitted Baskets](reports#submitted-baskets) 

	* [Voucher Performance](reports#voucher-performance) 

	* [Offer Performance](reports#offer-performance) 

	* [Statistics](reports#statistics) <br></br>


* [Stripe Balance](stripe) 

	* [Stripe Balance](stripe#stripe-balance) 

	* [How to View Stripe Transactions](stripe#how-to-view-stripe-transactions) 

	* [How to Add Stripe Balance](stripe#how-to-add-stripe-balance) <br></br>

## Administration Guide Version 2

* [Promotions](promov2)

	* [Promotions](promov2#promotions)

	* [How to Create Range for All Products](promov2#how-to-create-range-for-all-products)

	* [How to Create Range for Specific Products or Services](promov2#how-to-create-range-for-specific-products-or-services)

	* [How to Edit Ranges](promov2#how-to-edit-ranges)

	* [How to Delete Ranges](promov2#how-to-delete-ranges)

	* [Creating New Promo Code](promov2#creating-new-promo-code)

	* [How to View Details of your Promo Code](promov2#how-to-view-details-of-your-promo-code)

	* [How to Edit Promo Code](promov2#how-to-edit-promo-code)

	* [How to Delete Promo Code](promov2#how-to-delete-promo-code) 	<br></br>

* [Time Padding](paddingv2)

	* [How Time Padding Works](paddingv2#how-time-padding-works)

	* [How to Compute for Time Padding](paddingv2#how-to-compute-for-time-padding)

	* [Types of Time Padding](paddingv2#types-of-time-padding)

	* [Hierarchy of Time Padding Effect](paddingv2#hierarchy-of-time-padding-effect) <br></br>

* [Product Management](skillsandbookable)

	* [How to Create New Products with Version 2](skillsandbookable#how-to-create-new-products-with-version-2) 

	* [How to Create Skills Offered](skillsandbookable#how-to-create-skills-offered) 

	* [How to Edit Skills Offered](skillsandbookable#how-to-edit-skills-offered) 

	* [How to Edit Skills Offered](skillsandbookable#how-to-delete-a-skill) <br></br>


* [All About Stylist](stylistv2)

	* [All About Stylist Management New Features](stylistv2#all-about-stylist-management-new-features)

	* [How to Change Attending Stylist of a Certain Appointment by Skill](stylistv2#how-to-change-attending-stylist-of-a-certain-appointment-by-skill)

	* [How to see Stylist Change History](stylistv2#how-to-see-stylist-change-history)

	* [How to Update Stylist Bank Information](stylistv2#how-to-update-stylist-bank-information)

	* [How to Create Stylist Color](stylistv2#how-to-create-stylist-color)

	* [How to Edit Stylist Color](stylistv2#how-to-edit-stylist-color)

	* [Stylist Schedule Management](stylistv2#stylist-schedule-management)

	* [How to Create Stylist Work Schedules](stylistv2#how-to-create-stylist-work-schedules)

	* [How to Edit Stylist Work Schedules](stylistv2#how-to-edit-stylist-work-schedules)

	* [How to Delete Stylist Work Schedules](stylistv2#how-to-delete-stylist-work-schedules)

	* [How to Create Unavailable Schedule for Stylist](stylistv2#how-to-create-unavailable-schedule-for-stylist)

	* [How to Edit Unavailable Schedule for Stylist](stylistv2#how-to-edit-unavailable-schedule-for-stylist)

	* [How to Delete Unavailable Schedule for Stylist](stylistv2#how-to-delete-unavailable-schedule-for-stylist)

	<br></br>

## IOS Pageboy Guide

* [User Guide](userios) 

	* [Pre-requisite](userios#pre-requisite) 

	* [How to Register through Facebook Account](userios#how-to-register-through-facebook-account) 

	* [How to Register Using Email Address](userios#how-to-register-using-email-address) 

	* [Understanding Pageboy Menu](userios#understanding-page-boy-menu) 

	* [How to Browse Styles](userios#how-to-browse-styles) 

	* [How to Book an Appointment](userios#how-to-book-an-appointment) 

	* [How to View Your Calendar](userios#how-to-view-your-calendar) 

	* [How to Cancel or Modify an Appointment](userios#how-to-cancel-or-modify-an-appointment) 

	* [How to Take the Survey](userios#how-to-take-the-survey) <br></br>


* [Stylist Guide](stylistios) 

	* [Pre-requisite Set Up for a Stylist Account](stylistios#pre-requisite-set-up-for-a-stylist-account) 

	* [Register As Stylist](stylistios#register-as-stylist) 

	* [Setting Up Your Stylist Account via App](stylistios#setting-up-your-stylist-account-via-app) 

	* [How to Update Stylist Bank Account via App](stylistios#how-to-update-stylist-bank-account-via-app)

	* [Setting UP Your Stylist Schedule via App](stylistios#setting-up-your-stylist-schedule-via-app)  

	* [How to Edit Your Stylist Schedule via App](stylistios#how-to-edit-your-stylist-schedule-via-app) 

	* [How to Schedule Unavailable Time](stylistios#how-to-schedule-unavailability-time) <br></br>