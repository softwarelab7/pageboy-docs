### Dashboard

* Store Statistics - This is where you will see the Total Orders made on your IOS app, New Customers, Total Customers and Total Products you have. You will see the data within the day, including the graph of orders within time intervals. 

* Orders - Last 24 Hours - This is where you willl see all the orders made on your IOS Pageboy app within 24 hrs. 

* Orders - All Time - This is where you will see the total orders made on your IOS app, from the day you started using the app up to present.

* Customers - This is where you will see the total number of customers you have on your IOS app. You will also see if there are new customers within 24 hrs.

* Catalogue and Stocks - This is where you will see all the stocks you have and products offered.

* Offers, vouchers and promotions - This is where you will see all the active promotions you have, all discounts or vouchers you are offering that are currently active on the site. <br></br>


### Appointment Status of an Order

There are 2 Status for an appointment of an order or service or product.

1. Finished - if the order was done or finished by the stylist.

2. Cancelled - if the customer cancelled the appointment via Pageboy app or if they call in to cancel the appointment. <br></br>

There are 2 types of appointment cancellation.

* Cancellation before 3hrs of the appointment time and date - This will have no cost and will have full refund.

* Cancellation of appointment within 3 hrs or less of the appointment time and date, this will be charged 100% of the whole service/product. <br></br>


### Time Padding

There are 2 types of time padding that we have on this system.

1. Appointment Padding - This is the time interval for booking appointments. It is normally set in minutes and used for booking time interval from one Booking schedule to the next booking schedule.

	* Example: Appointment padding is set to 15 minutes. The booking times will start at 7am-8am, the next booking schedule will be 8:15am to 9:15am, the next one will be 9:30am to 10:30am and so on and so forth.  <br></br>

2. Set Padding From Current Date and Time - This is the time interval for a user before he/she would be able to book an appointment from the current time. It is normally set in minutes.

	* Example: The time as of now is 7am and you are using your Pageboy App, if the Set Padding From Current Date and Time is 60 minutes, you can only book 8am schedules onward. <br></br>

### Promotions

* In creating promotions there are 2 forms that you will need to create. 

	1. **Ranges** - It defines the product/s or service/s that will be under promotion.

	2. **Promo Code** - It defines the mechanics of the promotion you want to launch. 

NOTE: Create _**Ranges**_ first before creating a _**Promo Code**_.  <br></br>

### Reports

* There are different types of report that can be generated from the system.

	* Ordered Placed - This report is where you will see all orders made on the app with their details, from the time duration you have put in on Date From and Date to. <br></br>

	* Product Analytics - This report is where you will see all the products, how many times they are viewed and how many times they are ordered, from the time duration you have put in on Date From and Date to. This report is very useful to see what product sells most. <br></br>

	* User Analytics - This report is where you will see all the users, date of registration, product views and their purchase made on the app, from the time duration you have put in on Date From and Date to. <br></br>

	* Open Baskets - This report is where you will see all the open baskets or orders from your app, from the time duration you have put in on Date From and Date to. <br></br>

	* Submitted Baskets - This report is where you will see all the submitted baskets or orders from your app, from the time duration you have put in on Date From and Date to. <br></br>

	* Voucher Performance - This report is where you will see how many times your promotion/voucher was used and added to a basket. How much discount was used. This will reflect the promotion/voucher performance from the time duration you have put in on Date From and Date to. <br></br>

	* Offer Performance - This reports is where you can see the promotions you have offered from the time duration you have put in on Date From and Date to. This report will show you the amount of discounts your promotion/voucher have reached. <br></br>

	* Statistics - This report is where you can see the breakdown of all the orders made on your app. How many are Confirmed, cancelled, pending and processed. This includes the total revenue and the total orders made on the app. <br></br>

### Stripe Balance

* Stripe - it is a payment gateway that handles the process the payments made by Pageboy customers to Pageboy Bank account. Weekly all cleared payments are transferred to Pageboy Bank Account. <br></br>

* Stripe Balance - it is where we can see pending and processed payments from Pageboy customers. We need Stripe Balance to have certain amount daily to make sure that Stripe can process the payments for Gratuity of our Stylists on a daily basis depending on the services they catered.  <br></br>

### All about Stylist Management

* Partners - These are the stylists.  <br></br>

* Stylist Coverages - These are the coverage set by the stylist, how far they can go to cater the service needed in radius. <br></br>

* Stylist Schedules - These are the booked schedules of the stylist for the day, week or month. <br></br>

* Stylist Work Schedules - These are the schedules set by the stylist on their Pageboy App. <br></br>

* Stylist Bank Accounts - This is set by the stylist on their Pageboy app. This is where they are going to be paid. <br></br>

### User Status

* Active User - Active Pageboy app users. These users have the access to Pageboy app.

	Active Facebook User Account

	![Users](/img/users3.png) <br></br>

	Active Regular User Account

	![Users](/img/users4.png) <br></br>

* Inactive User - Inactive Pageboy app users. These users used to have access to Pageboy app but deactivated their account so they don't have access to Pageboy app anymore.

	![Users](/img/users5.png) <br></br>

* Waitlist Users - Users that tried to register on Pageboy app but are out of coverage area of our services. This page is usefull to see what location/Zip code have bulk of users wanting our service. <br></br>

### Types of Users

* Staff User - Admin users, the user that can and have access to manage the site.

	The example user is an active staff/admin that is also a stylist that the admin reviewed.

	![Users](/img/users7.png) <br></br>

* Regular User - Pageboy app customers.

	![Users](/img/users4.png) <br></br>

* Stylist Account - Stylist of Pageboy app.

	The example user is an active staff/admin that is also a stylist that the admin reviewed.

	![Users](/img/users6.png) <br></br>

* Set As Reviewed - Users of the app that said that they are stylist. Admin or staff users will need to review if they are really one of their stylist.

	The example user is an active staff/admin that is also a stylist that the admin reviewed.

	![Users](/img/users6.png) <br></br>


### Survey

_**Survey Rating**_

* 4.1-5.0 rating should have a weight of 4 - Gold <br></br>

* 3.1-4.0 rating should have a weight of 3 - Somewhat of a Premium Partner. <br></br>

* 2.1-3.0 rating should have a weight of 1.5 - Must have better chances still. <br></br>

* 1.1-2.0 rating should have a weight of 1 - Must have better odds than worst offenders. <br></br>

* 0.0-1.0,no ratings should have a weight of 0.5 - Even the worst offenders should have a chance. <br></br>