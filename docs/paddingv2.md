### How Time Padding Works

This time padding is set for the Stylist time buffer before attending his/her next appointment. It is used for them to have enough time to attend the next appointment. 

* **Attributes** - is the time needed for the service to be delivered or done by the stylist. <br></br>

* **Time padding** - is the time buffer for stylist before appointments. <br></br>

	* Product Padding <br></br>
	* Skill Padding <br></br>
	* Appointment Padding <br></br>

### How to Compute for Time Padding

General Equation:

* Next appointment for stylist = Attributes + Time padding + Time padding  <br></br>

Example 1: 

* Attribute = 10 am to 10:45 am
* Time Padding = 15 min

	* Next possible appointment = 10:45 am + 15 min + 15 min <br></br>

	* Next possible appointment = 11:15 am <br></br>

Example 2:

* Attribute = 10 am to 10:45 am
* Time Padding = 20 min

	* Next possible appointment = 10:45 am + 20 min + 20 min <br></br>

	* Next possible appointment = 11:30 am <br></br> the answer should be 11:25 am but since all our appointments starts every quarter hour, it will be rounded off to 11:30am  <br></br>

	Note: Time schedules for booking always starts every quarter of an hour. So normally customers will see 7am, 7:15am, 7:30am and 7:45a, then 8am something like that. <br></br>



### Types of Time Padding

There are 4 types of time padding that we have on this system.

1. _**Product Padding**_ - This is the time interval for booking appointments. It is normally set in minutes and used for booking time interval from one Booking schedule to the next booking schedule. This one only affects the _**PRODUCT**_ itself. <br></br>

	* Example: Product Padding is set to 15 minutes. The booking time will start at 10am and ends at 10:45am, the next booking time for the stylist will be 11:30am, so on and so forth. <br></br>

2. _**Skill Padding**_ - This is the time interval for booking appointments. It is normally set in minutes and used for booking time interval from one Booking schedule to the next booking schedule. This one only affects the _**Product with this certain Skill alone**_. <br></br>

	* Example: Skill Padding is set to 15 minutes. The booking time will start at 10am and ends at 10:45am, the next booking time for the stylist will be 11:30am, so on and so forth. <br></br>

3. _**Appointment Padding**_ - This is the time interval for booking appointments. It is normally set in minutes and used for booking time interval from one Booking schedule to the next booking schedule. <br></br>

	* Example: Appointment padding is set to 15 minutes. The booking times will start at 7am-8am, the next booking schedule will be 8:30am and so on and so forth.  <br></br>

4. _**Set Padding From Current Date and Time**_ - This is the time interval for a user before he/she would be able to book an appointment from the current time. It is normally set in minutes. <br></br>

	* Example: The time as of now is 7am and you are using your Pageboy App, if the Set Padding From Current Date and Time is 60 minutes, you can only book 8am schedules onward. <br></br>


### Hierarchy of Time Padding Effect

1. Product padding will always be first to take effect than the other padding. <br></br>

	* Example: If Product padding is set to 15 min, Skill padding is set to 30 min and Appointment padding is set to 15 min. Product padding will take effect not the skill padding and the appointment padding. If the schedule is set from 10am to 10:45am, the next appointment will be 11:30am.  <br></br>

2. Skill padding will take effect if there are no product time padding that is set to product padding. <br></br>

	* Example: If Product padding is not set, Skill padding is set to 30 min and Appointment padding is set to 15 min. Skill padding will take effect not the product padding and the appointment padding. If the schedule is set from 10am to 10:45am, the next appointment will be 11:45am.  <br></br>

	Note: if there are more than 1 skill padding present because the product have 2 skills in it, the highest time will be used as time padding. <br></br>

3. Appointment padding will take effect if there are no Product and Skill padding set for the product and skill paddings. <br></br>

	* Example: If Product padding is not set, Skill padding is not set and Appointment padding is set to 15 min. Appointment padding will take effect not the product padding and skill padding. If the schedule is set from 10am to 10:45am, the next appointment will be 11:15am.  <br></br>


