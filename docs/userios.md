### Pre-requisite 

1. Install Pageboy App.

2. Register through Facebook or Register using your email address.


### How to Register through Facebook Account

1. Download Pageboy App using your IOS Phone. Then install it.

	![Pageboy User](/img/icon.png) <br></br>

2. Press _**LOG IN WITH FACEBOOK**_.

	![Pageboy User](/img/registrationios1.png) <br></br>

3. You will be directed to a page where you are asked if you will authorize Pageboy to access your information via Facebook. Press _**Accept**_, you will be directed to a page where you are asked if you want to open the page in Pageboy.

	![Pageboy User](/img/registration4.png) <br></br>

4. Press _**Open**_ to open the go to the landing page.

	![Pageboy User](/img/landingpage.png) <br></br>



### How to Register Using Email Address

1. Download Pageboy App using your IOS Phone. Then install it.

	![Pageboy User](/img/icon.png) <br></br>

2. Press _**Register Now**_.

	![Pageboy User](/img/registrationios.png) <br></br>

3. Fill out the information needed for registration.

	![Pageboy User](/img/registration.png) <br></br>

4. Press _**Let's Go**_.

	![Pageboy User](/img/registration1.png) <br></br>

5. There are 2 output that will be seen on your IOS App. If you are under service coverage you will see the image below.

	![Pageboy User](/img/registration2.png) <br></br>

	If you are out of service coverage, you will see the image below.

	![Pageboy User](/img/raincheck.png) <br></br>


	* Note: If the registration is successful you will see the landing page. <br></br>

	![Pageboy User](/img/landingpage.png) <br></br>


### Understanding Pageboy Menu

1. Sign In to Pageboy App.

	![Pageboy User](/img/registrationios2.png) <br></br>

	You will be directed to this page.

	![Pageboy User](/img/signin.png) <br></br>

2. Press _**I'M BACK**_.

	![Pageboy User](/img/signin.png) <br></br>

	You will be directed to this page.

	![Pageboy User](/img/landingpage.png) <br></br>

3. Press _**=**_ on the upper right hand corner of the app.

	![Pageboy User](/img/landingpage1.png) <br></br>

	![Pageboy User](/img/landingpage2.png) <br></br>

	Here you will see the Menu of Pageboy App. <br></br>

4. Menu Content.

	* _**WHAT IS PAGEBOY**_ - this will tell you all about Pageboy. <br></br>

	![Pageboy User](/img/whatispageboy.png) <br></br>

	![Pageboy User](/img/whatispageboy1.png) <br></br>

	![Pageboy User](/img/whatispageboy2.png) <br></br>

	![Pageboy User](/img/whatispageboy3.png) <br></br>

	* _**STYLE GALLERY**_ - you will see here all the hair styles offered by Pageboy. <br></br>

	![Pageboy User](/img/gallery1.png) <br></br>

	Slide to the left to browse styles

	![Pageboy User](/img/gallery2.png) <br></br>

	![Pageboy User](/img/gallery3.png) <br></br>

	* _**MODIFY APPT**_ - this is where you can cancel your booked appointment. <br></br>

	![Pageboy User](/img/modifyapp.png) <br></br>

	* _**PRIMP MY EVENT**_ - this is where you can email Pageboy about your Event if you want to get their services. <br></br>

	![Pageboy User](/img/primpmyevent.png) <br></br>

	* _**VIEW YOUR CALENDAR**_ - this is where you will see all the schedules you booked using this app, cancelled or confrimed. <br></br>

	![Pageboy User](/img/calendar.png) <br></br>

	* _**LOGOUT**_ - this is where you can logout from the app. <br></br>

	![Pageboy User](/img/logout.png) <br></br>


### How to Browse Styles

1. Sign In to Pageboy App.

	![Pageboy User](/img/registrationios2.png) <br></br>

	You will be directed to this page.

	![Pageboy User](/img/signin.png) <br></br>

2. Press _**I'M BACK**_.

	![Pageboy User](/img/signin.png) <br></br>

	You will be directed to this page.

	![Pageboy User](/img/landingpage.png) <br></br>

3. Press _**=**_ on the upper right hand corner of the app.

	![Pageboy User](/img/landingpage1.png) <br></br>

	![Pageboy User](/img/landingpage2.png) <br></br>

	Here you will see the Menu of Pageboy App. <br></br>

4. Press _**Style Gallery**_.

	![Pageboy User](/img/menu.png) <br></br>

	You will be directed to this page.

	![Pageboy User](/img/gallery1.png) <br></br>

5. Swipe to from _**Right to Left**_ to browse Styles.

	![Pageboy User](/img/gallery2.png) <br></br>


### How to Book an Appointment

1. Browse a Look you want to book. See [How to Browse Styles](#how-to-browse-styles)

2. Click _**BOOK THIS LOOK**_.

	![Pageboy User](/img/booking0.png) <br></br>

3. You will be directed to a Calendar, Now Choose a _**Date**_ you want the style to be delivered. 

	![Pageboy User](/img/booking.png) <br></br>

4. Press _**Search**_, you will be directed to a page where you can see all the available time slots on the system.

	![Pageboy User](/img/booking1.png) <br></br>

5. Press _**BOOK**_ then you will be directed to this page where you will be asked about your Hair Details and if you have any preferred stylist.

	![Pageboy User](/img/booking3.png) <br></br>

6. Press _**BOOK**_ again to be able to see your booking details. Here you will see the Style you are ordering, the appointment Date and Time and How much it will be, gratuity included.

	![Pageboy User](/img/booking4.png) <br></br>

7. Press _**CONFIRM**_, to confirm the appointment. You will be directed to this page.

	![Pageboy User](/img/booking5.png) <br></br>

8. Fill out your Personal Information. Then Press _**UPDATE**_.

	![Pageboy User](/img/booking6.png) <br></br>

9. You will be directed to Credit Card Information, Enter your credited Card details and the _**Promo Code**_ for style you are ordering if there are ongoing promotions you want to avail. Press _**CONFIRM**_.

	![Pageboy User](/img/booking8.png) <br></br>

	Your booking is now Successful, you will see this page.

	![Pageboy User](/img/booking9.png) <br></br>


### How to View Your Calendar

1. Sign In to Pageboy App.

	![Pageboy User](/img/registrationios2.png) <br></br>

	You will be directed to this page.

	![Pageboy User](/img/signin.png) <br></br>

2. Press _**I'M BACK**_.

	![Pageboy User](/img/signin.png) <br></br>

	You will be directed to this page.

	![Pageboy User](/img/landingpage.png) <br></br>

3. Press _**=**_ on the upper right hand corner of the app.

	![Pageboy User](/img/landingpage1.png) <br></br>

	![Pageboy User](/img/landingpage2.png) <br></br>

	Here you will see the Menu of Pageboy App. <br></br>

4. Press _**VIEW YOUR CALENDAR**_. You will be directed to this page.

	![Pageboy User](/img/calendar.png) <br></br>

	All that are red are past or cancelled appointments.

5. Press _**Eye Icon or the Schedule itself**_ to be able to see the appointment details.

	![Pageboy User](/img/calender1.png) <br></br>



### How to Cancel or Modify an Appointment

1. Sign In to Pageboy App.

	![Pageboy User](/img/registrationios2.png) <br></br>

	You will be directed to this page.

	![Pageboy User](/img/signin.png) <br></br>

2. Press _**I'M BACK**_.

	![Pageboy User](/img/signin.png) <br></br>

	You will be directed to this page.

	![Pageboy User](/img/landingpage.png) <br></br>

3. Press _**=**_ on the upper right hand corner of the app.

	![Pageboy User](/img/landingpage1.png) <br></br>

	![Pageboy User](/img/landingpage2.png) <br></br>

	Here you will see the Menu of Pageboy App. <br></br>

4. Press _**VIEW YOUR CALENDAR**_. You will be directed to this page.

	![Pageboy User](/img/calendar.png) <br></br>

	All that are red are past or cancelled appointments.

5. Press _**Eye Icon**_ to be able to see the appointment details.

	![Pageboy User](/img/calendar2.png) <br></br>

6. Press _**CANCEL**_ to cancel the appointment.

	![Pageboy User](/img/calendar3.png) <br></br>

	You will be directed to this page.

	![Pageboy User](/img/cancel.png) <br></br>

7. Press _**Confirm**_ to cancel the appointment.

	![Pageboy User](/img/cancel1.png) <br></br>

	You will be directed to this page confirming that the appointment is cancelled.

	![Pageboy User](/img/cancel2.png) <br></br>	



### How to Take the Survey

1. Sign In to Pageboy App.

	![Pageboy User](/img/registrationios2.png) <br></br>

	You will be directed to this page.

	![Pageboy User](/img/signin.png) <br></br>

2. Press _**I'M BACK**_.

	![Pageboy User](/img/signin.png) <br></br>

	You will be directed to this page.

	![Pageboy User](/img/landingpage.png) <br></br>

3. Press _**=**_ on the upper right hand corner of the app.

	![Pageboy User](/img/landingpage1.png) <br></br>

	![Pageboy User](/img/landingpage2.png) <br></br>

	Here you will see the Menu of Pageboy App. <br></br>

4. Press _**VIEW YOUR CALENDAR**_. You will be directed to this page.

	![Pageboy User](/img/survey.png) <br></br>

	All that are red are past or cancelled appointments.

5. Press _**Eye Icon or the Schedule itself**_ of the finished appointment you want to take a Survey of.

	![Pageboy User](/img/survey.png) <br></br>

	You will be directed to this page.

6. Here you can rate your Stylist by pressing the _**Hair Icon**_ in the middle of your screen. The highest rating is _**5 Hair Icons**_ while the lowest is _**0 to 1 Hair Icon**_.

	![Pageboy User](/img/survey1.png) <br></br>

	* 4.1-5.0 rating should have a weight of 4 - Gold <br></br>

	* 3.1-4.0 rating should have a weight of 3 - Somewhat of a Premium Partner. <br></br>

	* 2.1-3.0 rating should have a weight of 1.5 - Must have better chances still. <br></br>

	* 1.1-2.0 rating should have a weight of 1 - Must have better odds than worst offenders. <br></br>

	* 0.0-1.0,no ratings should have a weight of 0.5 - Even the worst offenders should have a chance. <br></br>

7. Press _**Submit**_ to submit the survey.

	![Pageboy User](/img/survey2.png) <br></br>

	You will be directed to this page.

	![Pageboy User](/img/survey3.png) <br></br>
