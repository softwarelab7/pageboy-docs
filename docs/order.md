### How to View Orders

1. First, log in using your Admin credentials.

2. Click _**Fulfillment**_.

	![coverage](/img/coverage.png) <br></br>

3. Click _**Orders**_.

	![Orders](/img/orders.png) <br></br>	

	Then you will be directed to this page.

	![Orders](/img/orders1.png) <br></br>

	* You will see order details, this is the page where you will see all the orders made on the app.

	* Order Number - This is a numbering system on how many orders are made.

	* Total Inc Tax - Total Price including tax.

	* Number of Items - This is the total number of services/products ordered.

	* Status - This will indicate if the order was fulfilled or not yet

	* Customer - This is the name of the customer who ordered the service.

	* Date of Purchase	- The date when the order was made on Pageboy App.	<br></br>

### How to View Order Details of Specific Order

1. First, log in using your Admin credentials.

2. Click _**Fulfillment**_.

	![coverage](/img/coverage.png) <br></br>

3. Click _**Orders**_.

	![Orders](/img/orders.png) <br></br>	

	Then you will be directed to this page.

	![Orders](/img/orders1.png) <br></br>

4. There are 2 ways on how you can view Order Details of a specific order you want to check.

	* Click the _**Order Number**_.

	![Orders](/img/orders2.png) <br></br>	

	* Click _**Actions**_ beside the order number you want to check, then click _**View Orders**_.

	![Orders](/img/orders3.png) <br></br>	

	* Any of the two ways will direct you to this page. Here you can see the complete order details of the specific order. 

		- **Customer Information** - This is where you will see customer's name and Email address of the person who ordered the service/product.

		![Orders](/img/orders4.png) <br></br>	

		- **Order Information** - This is where you will see the total price of the order, date it was purchased, time it was purchased and its status.

		![Orders](/img/orders5.png) <br></br>	

		- **Order Details** - This is where you can see the price of the product/service, if there are any promotions used by the customer and total amount paid by the customer.

		![Orders](/img/orders6.png) <br></br>

		**Payment tab** will show payment transactions. The Amount _**Transferred**_ is the money being transferred to the Stylist. The Amount _**Settled**_ is the money paid by the customer.

		![Orders](/img/orders7.png) <br></br>	

		**Discount Tab** will show you what promotions is or was used by the customer if there are any.

		![Orders](/img/orders8.png) <br></br>	

		**Notes Tab** this is where you will see any notes regarding the order. Please be reminded that after 5 minutes, any notes that is put on this tab will be a permanent note on the order.

		![Orders](/img/orders9.png) <br></br>	

### How to View Appointment Details of an Order

1. First, log in using your Admin credentials.

2. Click _**Fulfillment**_.

	![coverage](/img/coverage.png) <br></br>

3. Click _**Orders**_.

	![Orders](/img/orders.png) <br></br>	

	Then you will be directed to this page.

	![Orders](/img/orders1.png) <br></br>

4. Click _**Actions**_ beside the order number you want to check the appointment.

	![Orders](/img/orders11.png) <br></br>

5. You will see the appointment details of the order.

	![Orders](/img/orders12.png) <br></br>

	- You will see Customer information, Appointment information wherein you can see the order number (Order), the total payment made by the customer (Total Incl Tax), the date it was ordered to be done (Start and End time), Address where the appointment is or will be done, Phone number of the customer, look or service/product that was ordered, stylist who cattered the service or product, preferred stylist of the customer and the hair condition according to the customer.

	- Appointment Gratuity is the amount transfered to the stylist who tend the order/service/product.

	- Appointment Gratuity Transactions is when it was paid to the stylist.

	- Change appointment status is where you can change the appointment status, please note that after an hour that the whole appointment time is done, the appointment status will be automatically changed by the system to _**Finished**_.  <br></br>

6. Other way to view appointment is to follow [How to View Order Details of Specific Order](#how-to-view-order-details-of-specific-order) then click _**View Appointment**_.

	![Orders](/img/orders10.png) <br></br>

### How to View All Appointments

1. First, log in using your Admin credentials.

2. Click _**Fulfillment**_.

	![coverage](/img/coverage.png) <br></br>

3. Click _**Appointments**_.

	![Orders](/img/appointment1.png) <br></br>	

	Then you will be directed to this page.

	![Orders](/img/appointment.png) <br></br>

	Here you will see the following:

	* Order number -This is a numbering system on how many orders are made. <br></br>

	* Start Time and End time - This is the Start and End time of the appointment. <br></br>

	* Address - This is the address where the service/product will be served. <br></br>

	* Attending Stylist - Name of the attending Stylist. <br></br>

	* Ratings - Survey Ratings for the attending Stylist. <br></br>

	* Preferred Stylist - A note from the customer who he/she wants to be his/her stylist. <br></br>

	* Cancellation Datetime - The time when the customer cancelled his/her appointment, if the appointment got cancelled. <br></br>

	* Amount Refunded - The amount being brought back to the customer's bank account if the appointment was cancelled before 3hrs and Date Ordered. <br></br>

4. Click _**View**_ beside the order number to be able to see the specific schedule of an order.

	![Orders](/img/appointment2.png) <br></br>

	You will be directed to this page.

	![Orders](/img/appointment3.png) <br></br>


### Appointment Status of an Order

There are 2 Status for an appointment of an order or service or product.

1. Finished - if the order was done or finished by the stylist.

2. Cancelled - if the customer cancelled the appointment via Pageboy app or if they call in to cancel the appointment. There are 2 types of appointment cancellation.

	* Cancellation before 3hrs of the appointment time and date - This will have no cost and will have full refund.

	* Cancellation of appointment within 3 hrs or less of the appointment time and date, this will be charged 100% of the whole service/product.


### How to Cancel an Appointment via Web


1. First, log in using your Admin credentials.

2. Click _**Fulfillment**_.

	![coverage](/img/coverage.png) <br></br>

3. Click _**Orders**_.

	![Orders](/img/orders.png) <br></br>	

	Then you will be directed to this page.

	![Orders](/img/orders1.png) <br></br>

4. Click _**Actions**_ beside the order number you want to check the appointment.

	![Orders](/img/orders11.png) <br></br>

	You will be directed to this page.

	![Orders](/img/orders12.png) <br></br>

5. Click _**New Status**_, select _**Cancelled**_ this way you can cancel the appointment. 

	![Orders](/img/orders13.png) <br></br>

	Please let the customer calling in to be aware of our cancellation policy. 

	* Cancellation before 3hrs of the appointment time and date - This will have no cost and will have full refund.


	* Cancellation of appointment within 3 hours or less of the appointment time and date, this will be charged 100% of the whole service/product.

	If the customer calls in and cancels the appointment 3 hours or more before the appointment time and date, Click the check box _**No Fee**_. If the customer calls in less than 3hrs before the appointment time and date, _**Do Not**_ check the check box _**No Fee**_. <br></br>

	![Orders](/img/orders14.png) <br></br>

	* Always inform our customer about our cancellation policy and ask them if they really want to cancel before cancelling the appointment. <br></br>

	![Orders](/img/orders15.png) <br></br>
	

6. Click _**Change Status**_ to confirm the cancellation of appointment. You will see a confirmation like the image below.

	![Orders](/img/orders16.png) <br></br>



### How to Change Attending Stylist of a Certain Appointment

* Stylist of each order are randomly selected by the system. Sometimes we have customers who are requesting for a certain stylist or when our stylist have some emergency we will need to change the stylist attending to a certain appointment. Here is how to change attending stylist.

	1. First, log in using your Admin credentials.

	2. Click _**Fulfillment**_.

		![coverage](/img/coverage.png) <br></br>

	3. Click _**Orders**_.

		![Orders](/img/orders.png) <br></br>	

		Then you will be directed to this page.

		![Orders](/img/orders1.png) <br></br>

	4. Click _**Actions**_ beside the order number you want to check the appointment.

		![Orders](/img/orders11.png) <br></br>

	5. Click _**Change Stylist**_.

		![Orders](/img/orders17.png) <br></br>

		You will be directed to this page.

		![Orders](/img/orders18.png) <br></br>

	6. Click _**Stylist**_. You will see a drop down of all the stylist available for that time and date. 

		![Orders](/img/orders19.png) <br></br>

		Note: The list of names you will see on this dropdown, are the stylists who don't have any appointment at that date and time of the order. If there are none, it just means that all stylist have an appointment at that time. <br></br>

	7. Click the name of stylist, then click _**Save**_ to change the stylist.

		![Orders](/img/orders20.png) <br></br>

		You will see a confirmation that the stylist was changed. From Ashley Maine to Test Testing.

		![Orders](/img/orders21.png) <br></br>

