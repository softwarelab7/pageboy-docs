### Product Management (Catalogue)

*  Under Catalogue there are several categories where you can manage your products.

	1. Looks 

		* Add Product/s <br></br>

		* Edit Product Information

			* Product Details

			* Attributes (Time duration for the service to be done)

			* Images

			* Product Pricing <br></br>

		* Delete Product/s <br></br>

	2. Ranges <br></br>

### Looks Products/Services

* Looks - this is where you can add, edit or delete products/services that you offer on your app; this will be directly 
be reflected on your IOS Pageboy app.

	* Click _**Catalogue**_

		![Products/Services](/img/looks.png) <br></br>

	* Click _**Looks**_ . You will see the image below.

		![Products/Services](/img/looks1.png) <br></br>

	* From here you can Add new products, Edit products and Delete products. <br></br>


### How to Add New Service or Product


1. Click _**Catalogue**_ then click _**Looks**_, you will be directed to the image below.

	![Products/Services](/img/looks.png) <br></br>

	![Products/Services](/img/looks1.png) <br></br> 

2. Choose _**Create a new product of type**_ by clicking the drop down. Choose _**Look**_.

	![Products/Services](/img/looks2.png) <br></br>

3. Click _**+ New Look**_. You will be directed to this page below.

	![Products/Services](/img/looks3.png) <br></br>

	You will see Product Details

	![Products/Services](/img/looks4.png) <br></br>

	Note: There are 7 sections in creating new products, that will need to be filled out first before you can save and create a new product.


	- Product Details - This is where you put in details for your products. 

	- Categories - This is where you put in which category your product will be put in. 

	- Attributes - This is where you put in how many minutes the hair style or service duration is.

	- Images - This is where you add photo/s of the product/services offered.

	- Stock and Pricing - This is where you put prices to your product.

	- Variants - (For Future Use)

	- Upselling - (For Future Use)

	<br></br>

	![Products/Services](/img/looks6.png) <br></br>

4. Fill out **Product Details** section.

	![Products/Services](/img/looks5.png) <br></br>

	- **Title**- name that referred to product.

	- **UPC**- or **Universal Product Code**, this is very useful in creating barcode symbol. Make sure to put in UPC without spaces this can be used when creating product/service promotions when you want to put this on discounted price or sale. Make sure to put in unique UPC on every service/product you will create.

	- **Description**- refers to product description. This is the right venue to describe the look.

	 	**Important Note:** Refrain from copy pasting from web straight to this section, this results to rendering of style copied from online. To avoid this, paste your copied words to a text editor tool first, for Mac users use _**TextEdit**_. From a text editor tool, copy the words and then you may now paste it on this section.

	- **Is Discountable?**- this means you're allowing your product to participate on promotions that the site has to offer. If you think this applies to your product then toggle this option. 

	- **Is Active**- this means your product can be seen on the app. <br></br>

	

5. Click **Categories**, select one category from the dropdown beside category. As of now the default that you should select is Pageboy.

	![Products/Services](/img/looks7.png) <br></br>

6. Click **Attributes**, input the duration or the time span to accomplish the certain service/product.

	![prod](/img/prod3.png) <br></br>

7. Click **Images**, click _**Upload Image**_ and locate the photo of your product that you want to be posted.

	![Products/Services](/img/looks8.png) <br></br>

8. Click **Stock and Pricing**, fill in all the required fields:


	Fill out the following fields.

	- **Cost Price**- product gross price (including tax).
	- **Price (excl tax)** - product net price.
	- **Retail Price**- means recommended price of the product, this will serve as comparison of price only with existing similar products in the market. <br></br>

	![Products/Services](/img/looks9.png) <br></br>


	Optional fields that will be automatically filled out by te system:

	- **Partner**- refers to the available stylist. Simply choose one from the choices.
	- **SKU**- or **Stock Keeping Unit**, this is a code that consists of letters, numbers or symbols that uniquely identifies the product. Normally used in inventory data management.
	- **Cost Price**- product gross price (including tax).
	- **Price (excl tax)** - product net price.
	- **Retail Price**- means recommended price of the product, this will serve as comparison of price only with existing similar products in the market. <br></br>

	![Products/Services](/img/looks10.png) <br></br>

9. Click _**Save**_. A message will appear to confirm that your product is created, it must now added under **Products** table and also visible on the app.

	![Products/Services](/img/looks11.png) <br></br>


	**Note:** Along with confirmation message are the two different useful buttons such as:

	- **Edit again**- if you missed something, click this one.

	- **View it on the site**- to scan the product on main page, click this one. <br></br>

	![Products/Services](/img/looks12.png) <br></br>

	Below is how your new product on this web.

	![Products/Services](/img/looks13.png) <br></br>

	Below is how it will look on Pageboy IOS app.

	![Products/Services](/img/looks14.png) <br></br>


### How to Edit Product Information

1. Click _**Catalogue**_ then Click _**Looks**_, you will be directed to the image below.

	![Products/Services](/img/looks.png) <br></br>

	![Products/Services](/img/looks1.png) <br></br> 

2. Click _**Action**_ beside the service/product you want to edit information.

	![Products/Services](/img/looks15.png) <br></br>

3. Click _**Edit**_

	![Products/Services](/img/looks16.png) <br></br>

	You will be directed to this page.

	![Products/Services](/img/looks17.png) <br></br>

4. Here you can edit what section/s you want to edit. Just by clicking the section you want to edit.

	_**Edit Product Details**_ 

	![Products/Services](/img/looks17.png) <br></br>

	_**Attributes**_ (Duration of service/product)

	![Products/Services](/img/looks18.png) <br></br>

	_**Images**_ , to add picture just click on _**Upload Image**_, browse the image you want to add then click _**Save**_. To Delete picture, just click on the check box below _**Delete**_ then click _**Save**_.

	![Products/Services](/img/looks19.png) <br></br>

	_**Stock and Pricing**_ , Edit Price of your product by filling out the the boxes under Product and Price. Previous price will be see below it, Stock and Pricing.

	![Products/Services](/img/looks20.png) <br></br>

5. Click _**Save**_ to save all the changes made on any sections edited. Once saved, all changes will be uploaded to your Pageboy IOS app.

	![Products/Services](/img/looks21.png) <br></br>

	![Products/Services](/img/looks22.png) <br></br> 



### How to Delete Product/Service

1. Click _**Catalogue**_ then press _**Looks**_, you will be directed to the image below.

	![Products/Services](/img/looks.png) <br></br>

	![Products/Services](/img/looks1.png) <br></br> 

2. Click _**Action**_ beside the service/product you want to _**Delete**_. 


	![Products/Services](/img/looks23.png) <br></br> 

3. You will be directed to a confirmation page wherein you have to click _**Delete**_ if you want to delete the product.

	![Products/Services](/img/looks24.png) <br></br> 

	After clicking _**Delete**_, the product/service you deleted will be permanently removed from the web and Pageboy IOS app. You will see a confirmation like the picture below.

	![Products/Services](/img/looks25.png) <br></br> 

