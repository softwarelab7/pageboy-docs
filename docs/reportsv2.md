### Reports

* There are different types of report that can be generated from the system.

	* _**Appointment Set**_ - This report is where you will see all the orders made on the app with their details, from the time duration you have put in on the Date From to Date to. <br></br>

	* _**User Purchases**_ - This report is where you will see the customer's name, contact number, email, purchased product (Finished or Newly ordered but not cancelled.), and total spent of the customer with gratuity, from the time duration you have put in on the Date From to Date to. <br></br>

	* _**Looks and Appointment Counts**_ - This report is where you will see all the products/services on the system and how many orders was placed on each products, categorized if the appointment is cancelled or finished/new, from the time duration you have put in on the Date From to Date to. <br></br>	

	* _**Ordered Placed**_ - This report is where you will see all orders made on the app with their details; regardless if the order was cancelled finished or new; from the time duration you have put in on Date From to Date to. <br></br>

	* _**Revenues**_ - This report is where you will see the total service sold per product and the total services sold overall, from the time duration you have put in on Date From to Date to. <br></br>

	* _**Product Analytics**_ - This report is where you will see all the products, how many times they are viewed and how many times they are ordered, from the start the App is up to the time present. this report is not filtered by dates. This report is very useful to see what product sells most. <br></br>

	* _**User Analytics**_ - This report is where you will see all the users, date of registration, product views and their purchase made on the app, from the start the App is up to the time present. this report is not filtered by dates. <br></br>

	* _**Open Baskets**_ - This report is where you will see all the open baskets or orders from your app, from the time duration you have put in on Date From to Date to. <br></br>

	* _**Submitted Baskets**_ - This report is where you will see all the submitted baskets or orders from your app, from the time duration you have put in on Date From to Date to. <br></br>

	* _**Voucher Performance**_ - This report is where you will see how many times your promotion/voucher was used and added to a basket. How much discount was used. This will reflect the promotion/voucher performance from the start the App is up to the time present. this report is not filtered by dates. <br></br>

	* _**Offer Performance**_ - This reports is where you can see the promotions you have offered from the time duration you have put in on Date From and Date to. This report will show you the amount of discounts your promotion/voucher have reached. <br></br>

	* _**Statistics**_ - This report is where you can see the breakdown of all the orders made on your app. How many are Confirmed, cancelled, pending and processed. This includes the total revenue and the total orders made on the app. <br></br>

NOTE: User Analytics, Products Analytics and Voucher Performance Reports are not filtered by date. Those reports are records from the start the App is operating till the present time.


### How to Run Reports by Date Range

The reports that can be run by date range are the following reports:

1. Appointments Set

2. User Purchases

3. Looks and Appointment Counts

4. Order Placed

5. Revenues

6. Open Baskets

7. Submitted Baskets

8. Offer Performance

Here is how to run the reports on the system

1. Click Reports.

	![Reports](/img/report.png) <br></br>

	You will be directed to this page.

	![Reports](/img/report1.png) <br></br>

2. Click the box beside Report Type.

	![Reports](/img/report2.png) <br></br>

3. Choose from the drop down the report you want to run.

	![Reports](/img/report3.png) <br></br>

4. Select Date From and Date to by clicking the box beside them.

	![Reports](/img/report4.png) <br></br>

	And

	![Reports](/img/report5.png) <br></br>

5. Click Generate report.

	![Reports](/img/report6.png) <br></br>

	You will be directed to this page.

	![Reports](/img/report7.png) <br></br>

### How to Run Reports that don't require Date Range

The Reports that are for all time are the following reports: 

1. Product Analytics

2. User Analytics

3. Voucher Performance

Here is how to run these type of reports.

1. Click Reports.

	![Reports](/img/report.png) <br></br>

	You will be directed to this page.

	![Reports](/img/report1.png) <br></br>

2. Click the box beside Report Type.

	![Reports](/img/report2.png) <br></br>

3. Choose from the drop down the report you want to run.

	![Reports](/img/report8.png) <br></br>

4. Click Generate Report.

	![Reports](/img/report9.png) <br></br>

	You will be directed to this page.

	![Reports](/img/report10.png) <br></br>


### How to Download Reports

If you want to download the report that you are generating all you have to do is to click the small box beside Download before clicking **Generate Report**. 

![Reports](/img/report11.png) <br></br>

You will be prompted to save the report or to open the report.

![Reports](/img/report12.png) <br></br>

