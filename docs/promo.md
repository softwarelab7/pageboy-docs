### Promotions

* In creating promotions there are 2 forms that you will need to create. 

	1. **Ranges** - It defines the product/s or service/s that will be under promotion.

	2. **Promo Code** - It defines the mechanics of the promotion you want to launch. 

NOTE: Create _**Ranges**_ first before creating a _**Promo Code**_.

### How to Create Range for All Products

1. Click _**Catalogue**_.

	![Promotions](/img/promo.png) <br></br>

2. Click _**Ranges**_.

	![Promotions](/img/promo1.png) <br></br>

	You will be directed to this page.

	![Promotions](/img/promo2.png) <br></br>

4. Click _**+Create New Range**_. 

	![Promotions](/img/promo3.png) <br></br>

	You will see this form. Fill out Range Name, Description, Includes All Products?, Included Categories.

	![Promotions](/img/promo4.png) <br></br>

	- **Name** - Name of the product range for promotions.

	- **Description** - The description of the promotion you are creating.

	- **Includes All Products?** - If all Products/services will be on promotion.

	- **Included Categories** - Choose Pageboy as default Category. <br></br>

5. Click _**Save**_.

	![Promotions](/img/promo5.png) <br></br>


### How to Create Range for Specific Products or Services

1. Click _**Catalogue**_.

	![Promotions](/img/promo.png) <br></br>

2. Click _**Ranges**_.

	![Promotions](/img/promo1.png) <br></br>

	You will be directed to this page.

	![Promotions](/img/promo2.png) <br></br>

3. Click _**+Create New Range**_. 

	![Promotions](/img/promo3.png) <br></br>

	You will see this form. Fill out Range Name, Description, Includes All Products?, Included Categories.

	![Promotions](/img/promo4.png) <br></br>

	- **Name** - Name of the product range for promotions.

	- **Description** - The description of the promotion you are creating.

	- **Includes All Products?** - If all Products/services will be on promotion.

	- **Included Categories** - Choose Pageboy as default Category. <br></br>

4. Make sure to _**NOT**_ to click Includes All Products. Click _**Save and edit products**_.

	![Promotions](/img/promo6.png) <br></br>

5. Add UPC of the Products/Services you want to put on promotions.

	* Single Product

		![Promotions](/img/promo7.png) <br></br>

	* Two or More Products - Enter the Product/s UPC/s on Add products on a new line by pressing Enter.

		![Promotions](/img/promo8.png) <br></br>

	* _**OR**_ eperated by comma _**,**_.

		![Promotions](/img/promo9.png) <br></br>

6. Press _**Go**_, to save the range.

	![Promotions](/img/promo10.png) <br></br>

	Then you will see a page confirming that all stocks are in that promotion. 

	![Promotions](/img/promo11.png) <br></br>

7. Press _**Return to range list**_. 

	![Promotions](/img/promo12.png) <br></br>

	You will be directed to All the ranges made on the account.

	![Promotions](/img/promo13.png) <br></br>


### How to Edit Ranges

1. Click _**Catalogue**_.

	![Promotions](/img/promo.png) <br></br>

2. Click _**Ranges**_.

	![Promotions](/img/promo1.png) <br></br>

	You will be directed to this page.

	![Promotions](/img/promo2.png) <br></br>

3. Click _**Actions**_ beside the Range you want to edit.

	![Promotions](/img/promo14.png) <br></br>

4. Click _**Edit**_. This is applicable for ranges having all products.

	![Promotions](/img/promo15.png) <br></br>

	You will be directed to a page where you can edit details on your range.

	![Promotions](/img/promo16.png) <br></br>	

5. If you are editing Ranges for specific products. You will see _**Edit Products**_ aside from Edit. Click on _**Edit Products**_ to edit the products included on this range.

	![Promotions](/img/promo17.png) <br></br>	

	You will be directed to this page below.

	![Promotions](/img/promo18.png) <br></br>	

	After editing the products included, you can press Go to save the changes or press Edit range to edit the details for the promo. If you will click _**Edit Range**_ you will be directed to the page below.

	![Promotions](/img/promo19.png) <br></br>	

	Here you can edit the Range details. <br></br>	

6. After editing everything, press _**Save**_ to save all changes made.

	![Promotions](/img/promo20.png) <br></br>

	You will see this page after pressing save. This means that all changes has been saved.

	![Promotions](/img/promo21.png) <br></br>
	

### How to Delete Ranges

1. Click _**Catalogue**_.

	![Promotions](/img/promo.png) <br></br>

2. Click _**Ranges**_.

	![Promotions](/img/promo1.png) <br></br>

	You will be directed to this page.

	![Promotions](/img/promo2.png) <br></br>

3. Click _**Actions**_ beside the Range you want to Delete.

	![Promotions](/img/promo14.png) <br></br>

4. Click _**Delete**_.

	![Promotions](/img/promo22.png) <br></br>

5. You will be directed to a page asking you to confirm if you want to delete the range. Click _**Delete**_.

	![Promotions](/img/promo23.png) <br></br>

	You will see a confirmation message that the range is deleted.

	![Promotions](/img/promo24.png) <br></br>





### Creating New Promo Code

1. Click _**Promo Codes**_, you will be directed Promo Codes where you can see all the Promo codes created on the account.


	![Promotions](/img/promocode.png) <br></br>

2. Click _**+Create new promo code**_.

	![Promotions](/img/promocode1.png) <br></br>


3. You'll be then directed to **Create promo code** page 

	![Promotions](/img/promocode2.png) <br></br>

4. Complete the form with the following required fields:

	- **Name**- name of promo

	- **Code**- this is the code that will be used on the app (ex. PB)

	- **Start Datetime** & **End Datetime**- these fields pertains to start and end date/time of your promo duration, you can simply choose a date from the calendar and adjust the **Hour** and **Minute** (if necessary) and click _**Done**_ to finish.

		![Promotions](/img/promocode3.png) <br></br>

		After clicking the date you want the promo code will start, you will be asked what time it should start.

		![Promotions](/img/promocode4.png) <br></br>

		You will be be asked what specific time this will be done.

		![Promotions](/img/promocode5.png) <br></br>

		You will need to do the same thing on with the _**End Datetime**_.

	- **Usage**- this is where you set when will your promo takes effect and whose entitled to use it. Conditions are as follows, see the description of each below:

		- **Can be used once by one customer**- means the promo code can be applied for one time only by one specific customer.

		- **Can be used multiple times by multiple customers**- the promo code can be applied numerous times by numerous customers.

		- **Can only be used once per customer**- the promo code can be applied once for each one customer.

	- **Which Products Get A Discount?**- this pertains to products that are covered by your particular promo. The only data shown to this field is **All**, meaning all active products are captured by the promo.

	- **Discount Type**- this refers to types of discount that the system can only offer. Select one from these default given choices, description of each is explained below:

		- **Percentage off of products in range**- means concerned products are entitled of particular percentage (the one entered on _Discount Value_, ex. 15% off) of markdown when conditions are met.

		- **Fixed amount off of products in range**- means concerned products are entitled of specific amount (again, you will enter the amount on _Discount Value_ field, ex. $10 off) off of discount when conditions are met.

	- **Discount Value**- this is the percentage or amount of discount, whatever you enter on this field applies to your selected _Discount Type_. <br></br>



5. Click **Save** to finish. Newly created Promo code must now be added under **Promo Codes** table. <br></br>

	![Promotions](/img/promocode6.png) <br></br>

	Then you will see the promotion you created.

	![Promotions](/img/promocode7.png) <br></br>	

	Note: Promo Code Status will be automatically **Active** when the start time is on and will be automatically _**Inactive**_ when the end time is done.


### How to View Details of your Promo Code


1. Click _**Promo Codes**_, you will be directed Promo Codes where you can see all the Promo codes created on the account.


	![Promotions](/img/promocode.png) <br></br>

2. Click _**Action**_.

	![Promotions](/img/promocode8.png) <br></br>

3. Click _**Stats**_.

	![Promotions](/img/promocode9.png) <br></br>

	You will see the details of the promo code you want to see.

	![Promotions](/img/promocode10.png) <br></br>


### How to Edit Promo Code

1. Click _**Promo Codes**_, you will be directed Promo Codes where you can see all the Promo codes created on the account.


	![Promotions](/img/promocode.png) <br></br>

2. Click _**Action**_.

	![Promotions](/img/promocode8.png) <br></br>

3. Click _**Edit**_.

	![Promotions](/img/promocode11.png) <br></br>

	You will be directed to this page.

	![Promotions](/img/promocode13.png) <br></br>

4. Click _**Save**_. This will save all the changes made.

	![Promotions](/img/promocode14.png) <br></br>

	You will see a message that the promo code is updated.

	![Promotions](/img/promocode15.png) <br></br>



### How to Delete Promo Code


1. Click _**Promo Codes**_, you will be directed Promo Codes where you can see all the Promo codes created on the account.


	![Promotions](/img/promocode.png) <br></br>

2. Click _**Action**_.

	![Promotions](/img/promocode8.png) <br></br>

3. Click _**Delete**_.

	![Promotions](/img/promocode12.png) <br></br>

	You will be directed to this page.

	![Promotions](/img/promocode16.png) <br></br>

4. Click _**Delete**_.

	![Promotions](/img/promocode17.png) <br></br>

	You will be directed to a page where you can see the confirmation that the promo code is deleted.
	