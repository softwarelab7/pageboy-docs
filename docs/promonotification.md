### How to send Push Notification for Promotions

1. Click _**Promos**_.

	![Notifications](/img/notification.png) <br></br>

2. Click _**Push Notifications**_.

	![Notifications](/img/notification1.png) <br></br>

	You will be directed to this page.

	![Notifications](/img/notification2.png) <br></br>

3. Click _**+Create new scheduled push notification**_.

	![Notifications](/img/notification3.png) <br></br>

	You will be directed to this page.

	![Notifications](/img/notification4.png) <br></br>

4. Fillout Message, Schedule Date and Time when to send the push notification and the end date and time. 

	![Notifications](/img/notification4.png) <br></br>

	NOTE: The Message you will put in on the Message box will be the message all of Pageboy users will receive as push notification.

5. Click _**Save**_. 

	![Notifications](/img/notification5.png) <br></br>

	You will be directed to this page.

	![Notifications](/img/notification6.png) <br></br>


### How to Edit Push Notification for Promotions

1. Click _**Promos**_.

	![Notifications](/img/notification.png) <br></br>

2. Click _**Push Notifications**_.

	![Notifications](/img/notification1.png) <br></br>

	You will be directed to this page.

	![Notifications](/img/notification2.png) <br></br>

3. Click _**Actions**_.

	![Notifications](/img/notifcation7.png) <br></br>

4. Click _**Edit**_.

	![Notifications](/img/notification8.png) <br></br>

	You will be directed to this page.

	![Notifications](/img/notification9.png) <br></br>

5. Edit the Message or the start date and time or the end date and time of this notification, anything that you want to edit.

	![Notifications](/img/notification9.png) <br></br>

6. Click _**Save**_.

	![Notifications](/img/notification10.png) <br></br>

	You will be directed to this page.

	![Notifications](/img/notification11.png) <br></br>


### How to Delete Push Notification for Promotions

1. Click _**Promos**_.

	![Notifications](/img/notification.png) <br></br>

2. Click _**Push Notifications**_.

	![Notifications](/img/notification1.png) <br></br>

	You will be directed to this page.

	![Notifications](/img/notification2.png) <br></br>

3. Click _**Actions**_.

	![Notifications](/img/notifcation7.png) <br></br>

4. Click _**Delete**_.

	![Notifications](/img/notification12.png) <br></br>

	You will be directed to this page.

	![Notifications](/img/notification13.png) <br></br>

5. Click _**Delete**_.

	![Notifications](/img/notification14.png) <br></br>

	You will be directed to this page.

	![Notifications](/img/notification15.png) <br></br>

	