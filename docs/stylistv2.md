### All about stylist Management New Features

We have New Added Features for Stylist Management, here are as follows: 

1. Change Stylist by Skill. <br></br>

2. Change Stylist history can be seen on notes. <br></br>

3. Update bank information for Stylist Account. <br></br>

4. Create/Edit Stylist Color Coding on Stylist Schedules. <br></br>

5. Stylist Schedule Management. Stylist work schedule and Stylist Unavailable schedule can now be created/edited or deleted via Admin Web Dashboard. <br></br>

6. Stylist Ratings can now be seen separately.  <br></br>

7. How to view Stylist Addresses and Default Address. <br></br>

8. Stylist Radius information can now be seen via Stylist Coverages and Stylist Default address. <br></br>



	* Change Stylist by Skill.

	![Change Stylist](/img/changestylist.png) <br></br>

	![Change Stylist](/img/changestylist1.png) <br></br>

	* Change Stylist history can be seen on notes.

	![Change Stylist](/img/changestylist2.png) <br></br>

	* Update bank information for Stylist Account.

	![Change Stylist](/img/changestylist3.png) <br></br>

	* Create/Edit Stylist Color Coding on Stylist Schedules.

	![Change Stylist](/img/stylistcolorcoding.png) <br></br>

	* Stylist Schedule Management. Stylist work schedule and Stylist Unavailable schedule can now be created/edited or deleted via Admin Web Dashboard.

	![Change Stylist](/img/stylistworksched.png) <br></br>

	![Change Stylist](/img/stylistunavail.png) <br></br>

	* Stylist Ratings can now be seen separately. 

	![Change Stylist](/img/stylistrating.png) <br></br>

	* How to view Stylist Addresses and Default Address.

	![Change Stylist](/img/stylistaddresses.png) <br></br>

	![Change Stylist](/img/stylistdefault.png) <br></br>


	* Stylist Radius information can now be seen via Stylist Coverages and Stylist Default address.

	![Change Stylist](/img/stylistrange.png) <br></br>

	![Change Stylist](/img/stylistdefault.png) <br></br>


### How to Change Attending Stylist of a Certain Appointment by Skill

* Stylist of each order are randomly selected by the system. Sometimes we have customers who are requesting for a certain stylist or when our stylist have some emergency we will need to change the stylist attending to a certain appointment. Here is how to change attending stylist.

	1. First, log in using your Admin credentials.

	2. Click _**Fulfillment**_.

		![coverage](/img/coverage.png) <br></br>

	3. Click _**Orders**_.

		![Orders](/img/orders.png) <br></br>	

		Then you will be directed to this page.

		![Orders](/img/orders1.png) <br></br>

	4. Click _**Actions**_ beside the order number you want to check the appointment.

		![Orders](/img/orders11.png) <br></br>

	5. Click _**Change Stylist**_.

		![Change Stylist](/img/changestylist4.png) <br></br>

		You will be directed to this page. As you can see below, the current stylist name and details are listed on the page so that you can see who is the current stylist, appointment details, and the skill of the stylist. 

		![Change Stylist](/img/changestylist5.png) <br></br>

	6. Click _**Stylist**_. You will see a drop down of all the stylist available for that time and date. The name of the current stylist is listed below as well for your reference.

		![Change Stylist](/img/changestylist6.png) <br></br>

		Note: The list of names you will see on this dropdown, are the stylists who don't have any appointment at that date and time of the order. If there are none, it just means that all stylist have an appointment at that time. <br></br>

	7. Click the name of stylist, then click _**Save**_ to change the stylist.

		![Change Stylist](/img/changestylist7.png) <br></br>

		You will see a confirmation that the stylist was changed. From Ashley Maine to Test Testing.

		![Change Stylist](/img/changestylist8.png) <br></br>


### How to see Stylist Change History

1. First, log in using your Admin credentials.

2. Click _**Fulfillment**_.

	![coverage](/img/coverage.png) <br></br>

3. Click _**Orders**_.

	![Orders](/img/orders.png) <br></br>	

	Then you will be directed to this page.

	![Orders](/img/orders1.png) <br></br>

4. There are 2 ways on how you can view Order Details of a specific order you want to check.

	* Click the _**Order Number**_.

	![Orders](/img/orders2.png) <br></br>	

	* Click _**Actions**_ beside the order number you want to check, then click _**View Orders**_.

	![Orders](/img/orders3.png) <br></br>	

	* Any of the two ways will direct you to this page. Here you can see the complete order details of the specific order. 

		- **Customer Information** - This is where you will see customer's name and Email address of the person who ordered the service/product.

		![Orders](/img/orders4.png) <br></br>	

		- **Order Information** - This is where you will see the total price of the order, date it was purchased, time it was purchased and its status.

		![Orders](/img/orders5.png) <br></br>	

		- **Order Details** - This is where you can see the price of the product/service, if there are any promotions used by the customer and total amount paid by the customer.

		![Orders](/img/orders6.png) <br></br>

		**Payment tab** will show payment transactions. The Amount _**Transferred**_ is the money being transferred to the Stylist. The Amount _**Settled**_ is the money paid by the customer.

		![Orders](/img/orders7.png) <br></br>	

		**Discount Tab** will show you what promotions is or was used by the customer if there are any.

		![Orders](/img/orders8.png) <br></br>	

		**Notes Tab** this is where you will see any notes regarding the order and the change of stylist made on the order every change will be listed here, you can also add a note about the order or the change of stylist on this page. 

		![Change Stylist](/img/changestylist2.png) <br></br>

		NOTE: Please be reminded that after 5 minutes, any notes that is put on this tab will be a permanent note on the order.
	
### How to Update Stylist Bank Information

Updating Stylist Bank Information is used for bank account authentication for our stylist bank accounts, so that we can transfer their payments without any concerns. 

1. Click _**Stylist Bank Account**_.

	![Stylist](/img/stylist17.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylist18.png) <br></br>

2. Click _**Actions**_.

	![Stylist](/img/update.png) <br></br>

3. Click _**Update Bank Account**_.

	![Stylist](/img/update1.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/updatebankaccount.png) <br></br>

4. Fill out the form with the new bank account the stylist have, then click _**Save**_.

	![Stylist](/img/updatebankaccount.png) <br></br>

	Confirmation will be seen.

	![Stylist](/img/stylist23.png) <br></br>



### How to View Stylist Bank Information

1. Click _**Stylist Bank Account**_.

	![Stylist](/img/stylist17.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylist18.png) <br></br>

2. Click _**Actions**_.

	![Stylist](/img/update.png) <br></br>

3. Click _**View Stripe Account Details**_.

	![Stylist](/img/update2.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/update3.png) <br></br>



### How to Create Stylist Color

1. Click _**Schedules**_.

	![Stylist](/img/stylistcolors.png) <br></br>

2. Click _**Stylist Colors**_.

	![Stylist](/img/stylistcolors1.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistcolors2.png) <br></br>

3. Click _**+Create new color**_.

	![Stylist](/img/stylistcolors3.png) <br></br>

4. Click the hyperlink that says **"Click here to see Color codes"**.

	![Stylist](/img/stylistcolors4.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistcolors5.png) <br></br>


5. Browse the page until you can see the picture below. 

	![Stylist](/img/stylistcolors6.png) <br></br>

6. Pick the color you want, by clicking the **Color**.

	![Stylist](/img/stylistcolors7.png) <br></br>

	You will see the code above right after you click the color you want. 

	![Stylist](/img/stylistcolors8.png) <br></br>

7. Copy the code you will see below the small box of the color you have choosen. 

	![Stylist](/img/stylistcolors9.png) <br></br>

8. Go back to the browser where Pageboy Dashboard is at.

	![Stylist](/img/stylistcolors3.png) <br></br>

9. Paste the color code you have copied from the other webpage, to **Code**.

	![Stylist](/img/stylistcolors10.png) <br></br>

10. Choose the stylist you want to have the color from the stylist dropdown. Click **Stylist**.

	![Stylist](/img/stylistcolors11.png) <br></br>

11. Click _**Save**_ to save the changes.

	![Stylist](/img/stylistcolors12.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistcolors13.png) <br></br>	


	Note: You will see the stylist colors on **Dashboard/Stylists Schedules**, each colors represents a particular stylist. 

	![Stylist](/img/stylistcolors14.png) <br></br>	


### How to Edit Stylist Color

1. Click _**Schedules**_.

	![Stylist](/img/stylistcolors.png) <br></br>

2. Click _**Stylist Colors**_.

	![Stylist](/img/stylistcolors1.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistcolors2.png) <br></br>

3. Click _**Edit**_ beside the stylist you want to edit colors with.

	![Stylist](/img/stylistcolors15.png) <br></br>	

	You will be directed to this page.

	![Stylist](/img/stylistcolors16.png) <br></br>

4. Here you can edit the Color Code or the stylist for the color code. 

 	![Stylist](/img/stylistcolors16.png) <br></br>


5. Click _**Save**_, to save the changes.

 	![Stylist](/img/stylistcolors17.png) <br></br>

 	You will be directed to this page.

	![Stylist](/img/stylistcolors18.png) <br></br>

	Note: You will see the stylist colors on **Dashboard/Stylists Schedules**, each colors represents a particular stylist. 

	![Stylist](/img/stylistcolors14.png) <br></br>	


### Stylist Schedule Management

All Stylist Schedules can now be edited through Admin Access and will be directly implemented to Pageboy Mobile App.

1. Stylist Work Schedule.
	
	* Admin can now create stylist work schedule via web dashboard once they are set as reviewed.

	* Admin can now edit stylist work schedule via web dashboard once they are set as reviewed.

	* Admin can now delete stylist work schedule via web dashboard once they are set as reviewed. <br></br>	

2. Stylist Unavailable Schedule.

	* Admin can now create stylist unavailable schedule via web dashboard once they are set as reviewed.

	* Admin can now edit stylist unavailable schedule via web dashboard once they are set as reviewed.

	* Admin can now delete stylist unavailable schedule via web dashboard once they are set as reviewed. <br></br>	


### How to Create Stylist Work Schedules 

1. Click _**Schedules**_.

	![Stylist](/img/stylistcolors.png) <br></br>

2. Click _**Stylist Work Schedules**_.

	![Stylist](/img/stylistworkschedule.png) <br></br>

 	You will be directed to this page.

	![Stylist](/img/stylistworkschedule1.png) <br></br>

3. Click _**Create Stylist Work Schedules**_.

	![Stylist](/img/stylistworkschedule2.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistworkschedule3.png) <br></br>

4. Fill out the Start, End, Weekdays and Stylist you want the schedule to belong to. 


	![Stylist](/img/stylistworkschedule4.png) <br></br>

	Weekdays is a dropdown where you can select multiple days.

	![Stylist](/img/stylistworkschedule5.png) <br></br>

	Note: Only Reviewed stylist can be seen on Stylist dropdown.

	![Stylist](/img/stylistworkschedule6.png) <br></br>

5. Click _**Save**_, to save the changes. 

	![Stylist](/img/stylistworkschedule7.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistworkschedule8.png) <br></br>




### How to Edit Stylist Work Schedules


1. Click _**Schedules**_.

	![Stylist](/img/stylistcolors.png) <br></br>

2. Click _**Stylist Work Schedules**_.

	![Stylist](/img/stylistworkschedule.png) <br></br>

 	You will be directed to this page.

	![Stylist](/img/stylistworkschedule1.png) <br></br>

3. Click _**Edit**_ beside the name of the stylist you want to edit schedule.

	![Stylist](/img/stylistworkschedule9.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistworkschedule10.png) <br></br>

4. Click _**Action**_ beside the day of the work schedule you want to edit.

	![Stylist](/img/stylistworkschedule11.png) <br></br>

5. Click _**Edit**_.

	![Stylist](/img/stylistworkschedule12.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistworkschedule13.png) <br></br>

6. Edit the Start and/or End time and or the day you want to edit, this will supersede the initial schedule set for that day or days.

	![Stylist](/img/stylistworkschedule13.png) <br></br>

7. Click _**Save**_, this will save the changes you made on the schedule.

	![Stylist](/img/stylistschedule14.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistsworkschedule15.png) <br></br>




### How to Delete Stylist Work Schedules

1. Click _**Schedules**_.

	![Stylist](/img/stylistcolors.png) <br></br>

2. Click _**Stylist Work Schedules**_.

	![Stylist](/img/stylistworkschedule.png) <br></br>

 	You will be directed to this page.

	![Stylist](/img/stylistworkschedule1.png) <br></br>

3. Click _**Edit**_ beside the name of the stylist you want to edit schedule.

	![Stylist](/img/stylistworkschedule9.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistworkschedule10.png) <br></br>

4. Click _**Action**_ beside the day of the work schedule you want to delete.

	![Stylist](/img/stylistworkschedule11.png) <br></br>

5. Click _**Delete**_.

	![Stylist](/img/stylistworkschedule16.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistworkschedule17.png) <br></br>

6. Click _**Delete**_, this will confirm that you are deleting the schedule.

	![Stylist](/img/stylistworkschedule18.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistworkschedule19.png) <br></br>


### How to Create Unavailable Schedule for Stylist

1. Click _**Schedules**_.

	![Stylist](/img/stylistcolors.png) <br></br>

2. Click _**Stylist Schedules**_.

	![Stylist](/img/unvailsched.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/unavailsched1.png) <br></br>

3. Hover your mouse to the date you want to set unavailable schedule for stylist. You will see a _**+**_ sign.

	![Stylist](/img/unavailsched2.png) <br></br>

4. Click _**+**_ sign.

	![Stylist](/img/unavailsched3.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/unavailsched4.png) <br></br>

5. Fill out the Title of the unavailable schedule, you can use emoticons/emoji if you want too, Start date, End date and choose from the drop down the stylist you want the unavailable schedule for.

	![Stylist](/img/unavailsched5.png) <br></br>

6. Click _**Save**_, to save the unavailable schedule, this update will directly reflect on the stylist Pageboy mobile app as well.

	![Stylist](/img/unavailsched6.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/unavailsched7.png) <br></br>


### How to Edit Unavailable Schedule for Stylist

1. Click _**Schedules**_.

	![Stylist](/img/stylistcolors.png) <br></br>

2. Click _**Stylist Schedules**_.

	![Stylist](/img/unvailsched.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/unavailsched1.png) <br></br>

3. Click Unavailable Schedule you want to edit from the calendar.

	![Stylist](/img/unavailsched8.png) <br></br>

4. Click _**Blue description of the unavailable schedule**_.

	![Stylist](/img/unavailsched9.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/unavailsched10.png) <br></br>

5. Click _**Edit**_.

	![Stylist](/img/unavailsched11.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/unavailsched12.png) <br></br>

6. Edit the detail you want to edit, like Title, Start date, Start time, End date and End time.

	![Stylist](/img/unavailsched12.png) <br></br>

7. Click _**Save**_, to save changes.

	![Stylist](/img/unavailsched13.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/unavailsched14.png) <br></br>



### How to Delete Unavailable Schedule for Stylist

1. Click _**Schedules**_.

	![Stylist](/img/stylistcolors.png) <br></br>

2. Click _**Stylist Schedules**_.

	![Stylist](/img/unvailsched.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/unavailsched1.png) <br></br>

3. Click Unavailable Schedule you want to delete from the calendar.

	![Stylist](/img/unavailsched8.png) <br></br>

4. Click _**Blue description of the unavailable schedule**_.

	![Stylist](/img/unavailsched9.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/unavailsched10.png) <br></br>

5. Click _**Delete**_.

	![Stylist](/img/unavailsched15.png) <br></br>

	You will see a pop up message.

	![Stylist](/img/unavailsched16.png) <br></br>

6. Click _**OK**_, this will delete the unavailable schedule permanently.

	![Stylist](/img/unavailsched17.png) <br></br>

	You will see a pop up message.

	![Stylist](/img/unavailsched18.png) <br></br>



### How to View Stylist Ratings

1. Click _**Fulfillment**_.

	![Stylist](/img/stylistrating1.png) <br></br>

2. Click _**Stylist Ratings**_. 

	![Stylist](/img/stylistrating2.png) <br></br>

	You will be directed to this page. Where you can see the name of the stylist, average rating per stylist, number of answered surveys per stylist and number of appointments they have handled.

	![Stylist](/img/stylistrating3.png) <br></br>


### How to Add/Create Stylist Address via Admin Web Dashboard

1. Click _**Users**_.

	![Stylist](/img/stylistaddresses1.png) <br></br>

2. Click _**Stylist Addresses**_.

	![Stylist](/img/stylistaddresses2.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistaddresses.png) <br></br>

3. Click _**+Create new stylist address**_.

	![Stylist](/img/stylistaddresses3.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistaddresses4.png) <br></br>

4. Fill out the form by putting in the stylist address, Address line 2 is for **Apartment/Suite Number or something like that**, Address, and from the dropdown the stylist.

	![Stylist](/img/stylistaddresses5.png) <br></br>

5. Click _**Save**_.

	![Stylist](/img/stylistaddresses6.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistaddresses7.png) <br></br>

NOTE: Automatically the newly saved Stylist address will be the stylist default address, that address will be the reference for stylist radius coverage and this change will automatically update the stylist address on Pageboy Mobile App.


### How to Edit Stylist Address via Admin Web Dashboard

1. Click _**Users**_.

	![Stylist](/img/stylistaddresses1.png) <br></br>

2. Click _**Stylist Addresses**_.

	![Stylist](/img/stylistaddresses2.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistaddresses.png) <br></br>

3. Click _**Actions**_ beside the name and address of the stylist you want to edit the address.

	![Stylist](/img/stylistaddresses8.png) <br></br>

4. Click _**Edit**_.

	![Stylist](/img/stylistaddresses9.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistaddresses10.png) <br></br>

5. Edit the address.

	![Stylist](/img/stylistaddresses11.png) <br></br>

6. Click _**Save**_.

	![Stylist](/img/stylistaddresses12.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistaddresses13.png) <br></br>


NOTE: Editing stylist address will not change the one that is on the stylist default address or on Pageboy Stylist Settings.


### How to Delete Stylist Address via Admin Web Dashboard

1. Click _**Users**_.

	![Stylist](/img/stylistaddresses1.png) <br></br>

2. Click _**Stylist Addresses**_.

	![Stylist](/img/stylistaddresses2.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistaddresses.png) <br></br>

3. Click _**Actions**_ beside the name and address of the stylist you want to delete the address.

	![Stylist](/img/stylistaddresses8.png) <br></br>

4. Click _**Delete**_.

	![Stylist](/img/stylistaddresses14.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistaddresses15.png) <br></br>

	Or this page, whenever you will delete a stylist address that have ordered anything from the systme as a customer or appointment when that was the stylist address the system will not let you delete the address.

	![Stylist](/img/stylistaddresses16.png) <br></br>

5. Click _**Delete**_, to permanently delete the address.

	![Stylist](/img/stylistaddresses17.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistaddresses18.png) <br></br>

### How to Set/Change/Edit Stylist Default Address

All Reviewed stylist will be on the list of Stylist Default Addresses regardless if they have a schedule or address put in on Pageboy App.

1. Click _**Users**_.

	![Stylist](/img/stylistaddresses1.png) <br></br>

2. Click _**Stylist Default Addresses**_.

	![Stylist](/img/stylistdefaultadd.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistdefaultadd1.png) <br></br>

3. Click _**Edit**_.

	![Stylist](/img/stylistdefaultadd2.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistdefaultadd3.png) <br></br>

4. Choose from the address dropdown the address you want the stylist to have as its default address.

	![Stylist](/img/stylistdefaultadd4.png) <br></br>

5. Click _**Save**_.

	![Stylist](/img/stylistdefaultadd5.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistdefaultadd6.png) <br></br>

### How to View Stylist Radius via Admin Web Dashboard

There are two ways on how to view Stylist Radius on Admin Web Dashboard.

* Stylist Coverage

	1. Click _**Fulfillment**_.

		![Stylist](/img/stylistrating1.png) <br></br>

	2. Click _**Stylist Coverage**_.

		![Stylist](/img/stylistcoverage.png) <br></br>

		You will be directed to this page.

		![Stylist](/img/stylistcoverage1.png) <br></br>

	3. Click _**Location Icon**_, the icons represents each reviewed stylist on registered on the system, the red circle represents their raduis of coverga. By clicking te icon, you will be able to see the name of the stylist, address and radius of coverage.

		![Stylist](/img/stylistcoverage2.png) <br></br>	

* Stylist Default Addresses

	1. Click _**Users**_.

		![Stylist](/img/stylistaddresses1.png) <br></br>

	2. Click _**Stylist Default Addresses**_.

		![Stylist](/img/stylistdefaultadd.png) <br></br>

		You will be directed to this page.

		![Stylist](/img/stylistdefaultadd1.png) <br></br>




