### All about Stylist Management

* Partners - These are the stylists.

* Stylist Coverages - These are the coverage set by the stylist, how far they can go to cater the service needed in radius. <br></br>

* Stylist Schedules - These are the booked schedules of the stylist for the day, week or month. <br></br>

* Stylist Work Schedules - These are the schedules set by the stylist on their Pageboy App. <br></br>

* Stylist Bank Accounts - This is set by the stylist on their Pageboy app. This is where they are going to be paid. <br></br>

### Partners or Stylists

1. Click _**Fulfillment**_.

	![coverage](/img/coverage.png) <br></br>

2. Click _**Partners**_.

	![Stylist](/img/stylist.png) <br></br>

	You will be directed to this page, wherein you will see all the active stylist on Pageboy App.

	![Stylist](/img/stylist1.png) <br></br>

Note: Stylist accounts are registered via Pageboy app. See [Register As Stylist](stylistios#register-as-stylist)


### Set Stylist Account As Reviewed

1. Click _**Users**_.

	![Users](/img/users.png) <br></br>

2. Click _**Users**_.

	![Users](/img/users1.png) <br></br>

	You will be directed to this page, where you will see all users on the system.

	![Users](/img/users2.png) <br></br>

3. Click _**Check box beside the email address**_ of the user you want to change _**Status**_ or _**User Type**_ or both.

	![Stylist](/img/stylist2.png) <br></br>

4. Click _**Go**_ to set the account as reviewed.

	![Stylist](/img/stylist2.png) <br></br>

	You will be directed to the same page but look at the account review status, it will be set to review.

	![Stylist](/img/stylist3.png) <br></br>

To check if everything is up and working for the stylist, go to [Partners](#partners-or-stylist). You should see the name of the person you have Set as Reviewed. Meaning you have confirmed that, that person is one of your stylist.

![Stylist](/img/stylist4.png) <br></br>

### How to Check Stylist Coverage

1. Click _**Fulfillment**_.

	![coverage](/img/coverage.png) <br></br>

2. Click _**Stylist Coverages**_. 

	![Stylist](/img/stylist5.png) <br></br>

	You will be directed to this page, wherein you can see a map and the radius of coverage of each of your stylist.

	![Stylist](/img/stylist6.png) <br></br>	

3. The red circles are your stylist coverage, now if the addresses of your stylist are near each other, they might overlapped. If that is the case, you may zoom in to see all the stylist coverage by scrolling your mouse forward or clicking _**+**_ sign on the lower right corner of your screen.

	![Stylist](/img/stylist6.png) <br></br>	

	These two addresses are near each other reason why they overlapped. We zoomed in so we can see each icon individually.

	Click the _**Icon**_ for you to be able to see the address and username of the stylist.

	![Stylist](/img/stylist7.png) <br></br>	

4. As we can see, we can only see red picture with icons since we have zoomed in to the red picture, now to be able to see the streets. Click _**Map**_. Then click _**Terrain**_. Then you will see the streets.

	![Stylist](/img/stylist8.png) <br></br>	


### How to View Stylist Schedules

* When a stylist is booked for a job, it will generate a schedule that we can see on the  web using admin/staff access. Here is how to view the booked schedules.

1.  Click _**Schedules**_.

	![Stylist](/img/stylist9.png) <br></br>	

2. Click _**Stylist Schedules**_.

	![Stylist](/img/stylist12.png) <br></br>	

	You will be directed to this page.

	![Stylist](/img/stylist13.png) <br></br>	

3. From here you can filter the name of the Stylist you want to check schedules for the month, week or day. You can also filter this via city and or service. Just click _**Filter**_ to be able to see the page you want to see.

	![Stylist](/img/stylist14.png) <br></br>	

	![Stylist](/img/stylist15.png) <br></br>	

	![Stylist](/img/stylist16.png) <br></br>	



### How to see Stylist Work Schedules

* Stylist Work Schedules are made by stylists on their IOS app. As admins/Staff we can also see their schedule via this web.

1.  Click _**Schedules**_.

	![Stylist](/img/stylist9.png) <br></br>	

2. Click _**Stylist Work Schedules**_.

	![Stylist](/img/stylist10.png) <br></br>	

	You will be directed to this page wherein you will see the schedules of all of your stylists.

	![Stylist](/img/stylist11.png) <br></br>	


### Stylist Bank Account

* Stylist Bank accounts are normally created using Pageboy App. On the web we can see if the stylist updated their account using Pageboy app or not yet. Their name will appear on the screen.

* See [Setting up your Stylist Account via App](stylistios#setting-up-your-stylist-account-via-app) for more details how to create one.

### How to Update Stylist Bank Account via Web

1. Click _**Stylist Bank Account**_.

	![Stylist](/img/stylist17.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylist18.png) <br></br>

2. Click _**Actions**_.

	![Stylist](/img/stylist19.png) <br></br>

3. Click _**Update Bank Account**_.

	![Stylist](/img/stylist20.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylist21.png) <br></br>	

4. Fill out the form with the new bank account the stylist have, then click _**Save**_.

	![Stylist](/img/stylist22.png) <br></br>

	Confirmation will be seen.

	![Stylist](/img/stylist23.png) <br></br>



