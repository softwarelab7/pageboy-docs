### Pre-requisite set up for a Stylist account.

1. Register as Stylist via Pageboy app.

2. Set up your Stylist account via Pageboy app.

3. Set up your work schedules via Pageboy app.

4. Wait for an Admin or Staff to confirm your status as a stylist. <br></br>




### Register As Stylist

1. Download Pageboy App using your IOS Phone. Then install it.

	![Stylist](/img/icon.png) <br></br>

2. Press _**Register Now**_.

	![Stylist](/img/registrationios.png) <br></br>

3. Fill out the information needed for registration.

	![Stylist](/img/registration.png) <br></br>

4. Press _**Let's Go**_.

	![Stylist](/img/registration1.png) <br></br>

	
5. There are 2 output that will be seen on your IOS App. If you are under service coverage you will see the image below.

	![Stylist](/img/registration2.png) <br></br>

	If you are out of service coverage, you will see the image below.

	![Stylist](/img/raincheck.png) <br></br>

6. The system will ask you to set up your own password and it will ask you if you are a stylist yes or no. If you are please press on _**I am a stylist**_. Then press _**ALL DONE**_.

	![Stylist](/img/registration3.png) <br></br>

	After the registration you will be directed to this landing page.

	![Stylist](/img/landingpage.png) <br></br>

### Setting up your Stylist Account via App

1. Log in using your stylist account. You will be directed to the landing page.

	![Stylist](/img/landingpage.png) <br></br>

2. Press _**=**_ on the upper right hand corner of the app.

	![Stylist](/img/landingpage1.png) <br></br>

3. Press _**Stylist Settings**_.

	![Stylist](/img/stylistios.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistios1.png) <br></br>

4. Press _**Location**_.

	![Stylist](/img/stylistios2.png) <br></br>

	Enter your location or address so that you can use it for the range radius.

	![Stylist](/img/stylistios3.png) <br></br>

	The system will search for your address.

	![Stylist](/img/stylistios4.png) <br></br>

5. Set the raduis of coverage you can cover as a stylist. It will be in miles.

	![Stylist](/img/stylistios5.png) <br></br>

6. Set your bank account by pressing Bank Account Number.

	![Stylist](/img/stylistios6.png) <br></br>

	You will be directed to this page. Press _**+**_ to add an account number.

	![Stylist](/img/stylistios7.png) <br></br>

7. Enter your bank account details. 

	![Stylist](/img/stylistios8.png) <br></br>

8. Press _**Done**_.

	![Stylist](/img/stylistios9.png) <br></br>

	You will see this page.

	![Stylist](/img/stylistios10.png) <br></br>

9. Press _**Bank**_.

	![Stylist](/img/stylistios13.png) <br></br>

10. Press _**UPDATE**_. This is to update the system of your information.

	![Stylist](/img/stylistios14.png) <br></br>

	Now your account is all set!

	![Stylist](/img/stylistios12.png) <br></br>

### How to Update Stylist Bank Account via App

1. Log in using your stylist account. You will be directed to the landing page.

	![Stylist](/img/landingpage.png) <br></br>

2. Press _**=**_ on the upper right hand corner of the app.

	![Stylist](/img/landingpage1.png) <br></br>

3. Press _**Stylist Settings**_.

	![Stylist](/img/stylistios.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistios32.png) <br></br>

4. Press _**Bank Account Number**_.

	![Stylist](/img/stylistios33.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistios34.png) <br></br>

5. Press _**+**_ on the upper right corner of the app.

	![Stylist](/img/stylistios35.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistios8.png) <br></br>

6. Press _**DONE**_ on the upper right corner of the screen.

	![Stylist](/img/stylistios9.png) <br></br>

7. Press New Account number first, then press _**<**_  on the upper left corner to go to Stylist account.

	![Stylist](/img/stylistios36.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistios37.png) <br></br>

8. Press _**UPDATE**_ to update the new bank account to your stylist account.

	![Stylist](/img/stylistios38.png) <br></br>


### Setting Up Your Stylist Schedule via App

1. Log in using your stylist account. You will be directed to the landing page.

	![Stylist](/img/landingpage.png) <br></br>

2. Press _**=**_ on the upper right hand corner of the app.

	![Stylist](/img/landingpage1.png) <br></br>

3. Press _**VIEW YOUR CALENDAR**_.

	![Stylist](/img/stylistios15.png) <br></br>

	You will see your calendar, this calendar is where you can see all of your appointments as a customer and stylist. Normally it is blank if it is a newly set up account.

	![Stylist](/img/stylistios16.png) <br></br>	

4. Press _**WORK SCHED**_ at the bottom middle of the screen, to add or view work schedules.

	![Stylist](/img/stylistios17.png) <br></br>	

5. Press _**EDIT**_ to add schedule.

	![Stylist](/img/stylistios18.png) <br></br>	

6. Add Schedule by pressing Start and Ends, including the days of Repeat. By Pressing on Start then turning the dial for the time, then Press End, turn dial to the time you want to set. Press the days you want the schedule to repeat. 

	![Stylist](/img/stylistios19.png) <br></br>	

7. Press _**DONE**_ to confirm the schedule for that day or days in repeat.

	![Stylist](/img/stylistios20.png) <br></br>	

	After pressing done you will see your work schedule posted.

	![Stylist](/img/stylistios21.png) <br></br>	



### How to Edit Your Stylist Schedule via App

1. Log in using your stylist account. You will be directed to the landing page.

	![Stylist](/img/landingpage.png) <br></br>

2. Press _**=**_ on the upper right hand corner of the app.

	![Stylist](/img/landingpage1.png) <br></br>

3. Press _**VIEW YOUR CALENDAR**_.

	![Stylist](/img/stylistios15.png) <br></br>

	You will see your calendar, this calendar is where you can see all of your appointments as a customer and stylist. Normally it is blank if it is a newly set up account.

	![Stylist](/img/stylistios16.png) <br></br>	

4. Press _**WORK SCHED**_ at the bottom middle of the screen, to add or view work schedules.

	![Stylist](/img/stylistios21.png) <br></br>	

5. Press _**Edit**_, to be able to change your work schedule set.

	![Stylist](/img/stylistios22.png) <br></br>	

6. Press Start and End time. Choose the day you want to edit.

	![Stylist](/img/stylistios23.png) <br></br>	

7. Press _**Done**_ once you are done editing.

	![Stylist](/img/stylistios24.png) <br></br>	

	You will be directed to the Work Sched. As you can see the date we have edited is now updated.

	![Stylist](/img/stylistios25.png) <br></br>		


### How to Schedule Unavailability Time.

* As we can see, our work schedule is on repeat, now if you want to go out for a day or for certain hours and don't want to move your work Schedule, you can always put Unavailability Time. This way you won't need to change your Work Schedule just apply unavailability time. <br></br>

* Here is how to do it.

	1. Log in using your stylist account. You will be directed to the landing page.

		![Stylist](/img/landingpage.png) <br></br>

	2. Press _**=**_ on the upper right hand corner of the app.

		![Stylist](/img/landingpage1.png) <br></br>

	3. Press _**VIEW YOUR CALENDAR**_.

		![Stylist](/img/stylistios15.png) <br></br>

	4. Press _**UNAVAILABILITY**_ located at the lower right corner of your screen.

		![Stylist](/img/stylistios26.png) <br></br>

	5. Press _**+**_ to add Unavailable Schedule.

		![Stylist](/img/stylistios27.png) <br></br>

		You will see the image below where you can set up your unavailable time.

		![Stylist](/img/stylistios28.png) <br></br>	

	6. Type in the Title of your Unavailable schedule, Slide if it is for whole day or not, and Press Starts and Ends to set the date and time.

		![Stylist](/img/stylistios29.png) <br></br>	

	7. Press _**DONE**_ after setting all up.

		![Stylist](/img/stylistios30.png) <br></br>	

		You will be directed to this page. Now everything is set, the system will not let you have an appointment during the day and time you have set on Unavailability.

		![Stylist](/img/stylistios31.png) <br></br>		