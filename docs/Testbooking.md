### Test Booking Schedule

This is use to test if the schedule time and date is still available for a certain product to be booked.

1. Click _**Schedules**_.

	![Test Booking](/img/test.png) <br></br>

2. Click _**Test Booking Schedule**_.

	![Test Booking](/img/test1.png) <br></br>

	You will be directed to this page.

	![Test Booking](/img/test2.png) <br></br>

3. Select Product from the dropdown, fill out address, select the start date and time and select the end date and time. 

	![Test Booking](/img/test2.png) <br></br>

4. Click _**Post**_, to search if the schedule for that product is bookable.

	![Test Booking](/img/test3.png) <br></br>

	You will be directed to this page for the answer.

	![Test Booking](/img/test4.png) <br></br>

	NOTE: There are couple of results that you might see, depending if it is out of coverage area, no stylist available or the schedule time is full.

	



