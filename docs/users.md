### User Status

* Active User - Active Pageboy app users. These users have the access to Pageboy app. <br></br>

	Active Facebook User Account

	![Users](/img/users3.png) <br></br>

	Active Regular User Account

	![Users](/img/users4.png) <br></br>

* Inactive User - Inactive Pageboy app users. These users used to have access to Pageboy app but deactivated their account so they don't have access to Pageboy app anymore. <br></br>

	![Users](/img/users5.png) <br></br>

* Waitlist Users - Users that tried to register on Pageboy app but are out of coverage area of our services. This page is usefull to see what location/Zip code have bulk of users wanting our service. <br></br>

### Types of Users

* Staff User - Admin users, the user that can and have access to manage the site. <br></br>

	The example user is an active staff/admin that is also a stylist that the admin reviewed.

	![Users](/img/users7.png) <br></br>

* Regular User - Pageboy app customers.

	![Users](/img/users4.png) <br></br>

* Stylist Account - Stylist of Pageboy app.

	The example user is an active staff/admin that is also a stylist that the admin reviewed.

	![Users](/img/users6.png) <br></br>

* Set As Reviewed - Users of the app that said that they are stylist. Admin or staff users will need to review if they are really one of their stylist. <br></br>

	The example user is an active staff/admin that is also a stylist that the admin reviewed.

	![Users](/img/users6.png) <br></br>


### User Management

User Management - this is the page where you can manage all users using your Pageboy app. <br></br>

1. Click _**Users**_.

	![Users](/img/users.png) <br></br>

2. Click _**Users**_.

	![Users](/img/users1.png) <br></br>

	You will be directed to this page, where you will see all users on the system.

	![Users](/img/users2.png) <br></br>


### How to Change User Status or Type

1. Click _**Users**_.

	![Users](/img/users.png) <br></br>

2. Click _**Users**_.

	![Users](/img/users1.png) <br></br>

	You will be directed to this page, where you will see all users on the system.

	![Users](/img/users2.png) <br></br>

3. Click _**Check box beside the email address**_ of the user you want to change _**Status**_ or _**User Type**_ or both. <br></br>

	To change Status of the user either Click _**Make active**_ or _**Make inactive**_. <br></br>


	To change User Type either Click _**Add staff status**_ or _**Remove staff status**_ or _**Make regular account**_ or _**Make stylist account**_. <br></br>

	To confirm if the user is really a stylist Click _**Set as reviewed**_. <br></br>

	
4. Click _**Go**_ to confirm the change of Status or Type. From inactive user to Active user.

	![Users](/img/users8.png) <br></br>

	You will see the change right after you press Go.

	![Users](/img/users9.png) <br></br>

NOTE: Same steps will apply to change any User _**Status or Type**_.


### Waitlist

1. Click _**Users**_.

	![Users](/img/users.png) <br></br>

2. Click _**Waitlist**_.

	![Users](/img/users10.png) <br></br>

	You will be directed to this page, where you can see all the customers who tried to use the app but is out of coverage area.

	![Users](/img/users11.png) <br></br>


### How to do Password Reset

1. Click _**Users**_.

	![Users](/img/users.png) <br></br>

2. Click _**Users**_.

	![Users](/img/users1.png) <br></br>

	You will be directed to this page, where you will see all users on the system.

	![Users](/img/users2.png) <br></br>

3. Click the email address of the user you want to do password reset with.

	![Users](/img/passwordreset.png) <br></br>

	You will be directed to this page.

	![Users](/img/passwordreset1.png) <br></br>

4. Click _**Send password reset email**_, this will trigger the system to send an email of the new password to the email address for the user.

	![Users](/img/passwordreset2.png) <br></br>






