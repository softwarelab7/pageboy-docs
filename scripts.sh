#!/bin/bash

function create-venv() {
	mkvirtualenv pageboy-docs
}

function venv-workon() {
	workon pageboy-docs
}
